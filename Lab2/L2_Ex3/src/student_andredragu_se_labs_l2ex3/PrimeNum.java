// Program care afiseaza numerele prime dintr-un interval specificat

package student_andredragu_se_labs_l2ex3;

import java.util.Scanner;

public class PrimeNum {
	
    public static void main(String args[]) {
    	
    	Scanner s = new Scanner(System.in);
        int a, b, flag = 0, i, j;				// Declararea variabilelor
        
        System.out.println ("Introduceti valoarea minima a intervalului: "); 		// Valoarea minima a intervalului
        a = s.nextInt();
        
        System.out.println ("Introduceti valoarea maxima a intervalului: "); 		// Valoarea maxima a intervalului
        b = s.nextInt();
        
        System.out.println ("Numerele prime din intervalul ales sunt: ");			// Afisarea numerelor prime din interval
        
        for(i = a; i <= b; i++) {			// Parcurgerea intervalului
            for( j = 2; j < i; j++) {			// Omiterea valorilor 0 si 1, deoarece nu sunt nici numere prime, nici neprime ca sa zic asa
                if(i % j == 0) {			// Daca numarul nu este prim: flag = 0
                    flag = 0;
                    break;
                }
                else {			// Daca numarul este prim: flag = 1
                    flag = 1;
                }
                
            }
            if(flag == 1) {			// Afisarea numerelor prime din interval
                System.out.println(i);
            }
            
        }
        
    }
    
}

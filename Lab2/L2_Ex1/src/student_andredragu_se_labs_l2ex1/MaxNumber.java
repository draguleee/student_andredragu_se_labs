// Program care citeste doua numere de la tastatura si afiseaza maximul dintre acestea

package student_andredragu_se_labs_l2ex1;

import java.util.Scanner;

public class MaxNumber {
	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
			int x = in.nextInt();							// Initializare primul numar
			int y = in.nextInt();							// Initializare al doilea numar
			
			if(x >= y)										// Conditia pentru ca x sa fie maximul
				System.out.println("Maximul este: " + x);
			else 											// Conditia pentru ca y sa fie maximul
				System.out.println("Maximul este: " + y);
	}
	
}

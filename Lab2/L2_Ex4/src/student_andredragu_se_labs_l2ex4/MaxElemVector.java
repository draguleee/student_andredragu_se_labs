// Program care afiseaza elementul maxim dintr-un vector

package student_andredragu_se_labs_l2ex4;

import java.util.Scanner;
import java.io.*;

public class MaxElemVector {
	
	public static void main(String[] args)throws IOException {
		Scanner s = new Scanner(System.in);
		
		int n;			// Initializarea numarului de elemente din vector
		System.out.print ("Introduceti numarul de elemente n = ");			// Introducerea numarului de elemente din vector
		n = s.nextInt();
		int V[]=new int[n];			// Reinitializare vector dupa stabilirea numarului de elemente din acesta
		
		int i;			// Initializare contor
		for(i = 0; i < n; i++) {			// Parcurgere vector pentru a afisa elementele acestuia
			System.out.print("V["+i+"]=");
			V[i]=s.nextInt();
		}
		
		int max=V[0];			// Initializarea maximului din vector
		for(i = 1; i < n; i++)			// Parcurgere vector
			if(max<V[i]) max=V[i];			// Conditia pentru maxim
		System.out.println("Valoarea maxima este "+max);			// Afisarea vectorului
	}
	
}

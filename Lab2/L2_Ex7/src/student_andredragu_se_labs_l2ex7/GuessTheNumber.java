// Program care genereaza un numar aleatoriu si roaga utilizatorul sa il ghiceasca.
// Daca utilizatorul ghiceste numarul, programul se va opri.
// Daca utilizatorul nu ghiceste numarul, programul va afisa un mesaj corespunzator.
// Programul va permite maximum 3 incercari, dupa care se va opri, afisand mesajul "Ai pierdut!".

package student_andredragu_se_labs_l2ex7;

import java.util.Scanner;
import java.util.Random;

public class GuessTheNumber {
	
	public static void main(String args[]) {
		
		Scanner s = new Scanner(System.in);
		int count = 0;			// Initializarea numarului de incercari pentru a ghici raspunsul corect
		int a = 1 + (int) (Math.random() * 99);			// Initializarea numarului care trebuie ghicit
		int guess = 0;			// Initializarea raspunsului corect, care va trebui sa fie egal cu numarul care trebuie ghicit (if that makes a bit sense)

		System.out.println("Ma gandesc la un numar intre 1 si 100. Ghiceste-l!");		// Un mesaj destul de impunator pentru a ghici acest numar generat aleatoriu

		while (guess != a) {		// Daca raspunsul nu este corect...
			guess = s.nextInt();		// ... se mai acorda o sansa pentru a-l ghici
			count++;				// Se incrementeaza numarul de incercari
			if (guess > a) {						// Daca numarul introdus este mai mare decat numarul care trebuie ghicit...
				System.out.println("Mai mic!");		// ... se va afisa un mesaj care iti va spune sa introduci un numar mai mic
			} 	
			else if (guess < a) {					// Daca numarul introdus este mai mic decat numarul care trebuie ghicit...
				System.out.println("Mai mare!");	// ... se va afisa un mesaj care iti va spune sa introduci un numar mai mare
			}
		}
		
		System.out.println("Bravo! Ai ghicit numarul din " + count + " incercari!");		// Felicitari! Ai ghicit numarul! Doamne ajuta!
	}
	
}

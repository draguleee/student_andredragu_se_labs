// Program care afiseaza factorialul unui numar
// Folosind metoda recursiva.

package student_andredragu_se_labs_l2ex6a;

import java.util.Scanner;

public class RecursiveFactorial {
	
	// Functia pentru calculul factorialului
    public static long multiplyNumbers(int n) {			
        if (n >= 1)			// Daca numarul este mai mare sau egal cu 1, se returneaza factorialul sau calculat cu formula specifica
            return n * multiplyNumbers(n - 1);
        else			// Daca numarul este mai mic decat 1 (adica 0 ce-i drept), se returneaza valoarea 1 (0! = 1)
            return 1;
    }
    
	// Functia main
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n;			// Declararea variabilei
        System.out.print("Introduceti numarul pentru care se va calcula factorialul: ");			// Introducere numar
        n = s.nextInt();
        long factorial = multiplyNumbers(n);			// Calcularea factorialului
        System.out.println("Factorialul numarului ales este: " + n + " = " + factorial);			// Afisarea factorialului
    }
    
}

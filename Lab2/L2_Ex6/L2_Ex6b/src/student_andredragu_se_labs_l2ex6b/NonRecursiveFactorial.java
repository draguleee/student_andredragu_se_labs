// Program care afiseaza factorialul unui numar
// Folosind metoda nerecursiva.

package student_andredragu_se_labs_l2ex6b;

import java.util.Scanner;

public class NonRecursiveFactorial {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int n, mul = 1;			// Declarare variabile
	    System.out.print("Introduceti numarul pentru care se va calcula factorialul: ");		// Introducere numar
	    n = s.nextInt();
	    for(int i = 1; i <= n; i++) {			// Calcularea factorialului
	    	mul = mul * i;
	    }
	    System.out.println("Factorialul lui "+n+" este:" + mul);			// Afisarea factorialului
	}
	
}

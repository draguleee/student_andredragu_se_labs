// Program care genereaza un vector de 10 elemente,
// Il sorteaza prin metoda bulelor si
// Afiseaza vectorul sortat.

package student_andredragu_se_labs_l2ex5;

import java.util.Scanner;

public class BubbleSort {
	
	// Functia care efectueaza bubblesort in vectorul dat
	void bubbleSort(int v[]) { 
		int n = v.length; 
		for (int i = 0; i < n-1; i++) 
			for (int j = 0; j < n-i-1; j++) 
				if (v[j] > v[j+1]){ 
					int temp = v[j]; 
					v[j] = v[j+1]; 
					v[j+1] = temp; 
				} 
	} 

	// Functia care afiseaza vectorul
	void printVector(int v[]) { 
		int n = v.length; 
		for (int i=0; i<n; ++i) 
			System.out.print(v[i] + " "); 
		System.out.println(); 
	} 

	// Functia main
	public static void main(String args[]) { 
		BubbleSort ob = new BubbleSort(); 
		Scanner s = new Scanner(System.in);
		int n;			
		System.out.print("Introduceti numarul de elemente n = ");			
		n = s.nextInt();
		int v[]=new int[n];	
		
		for(int i = 0; i < n; i++) {			
			System.out.print("v["+i+"]=");
			v[i]=s.nextInt();
		}
		
		ob.bubbleSort(v); 
		System.out.print("Vectorul sortat:"); 
		ob.printVector(v); 
	} 
	
}

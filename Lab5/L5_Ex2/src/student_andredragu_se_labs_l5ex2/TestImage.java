// Clasa TestImage 

package student_andredragu_se_labs_l5ex2;

public class TestImage {

	public static void main(String[] args) { 
		
		RealImage r1 = new RealImage("abc");
		RealImage r2 = new RealImage("def");
		RealImage r3 = new RealImage("ghi");
		
		r1.display();
		r2.display();
		r3.display();
		
		System.out.println(" ");
		
		ProxyImage p = new ProxyImage();
		p.display();
		
	}
}

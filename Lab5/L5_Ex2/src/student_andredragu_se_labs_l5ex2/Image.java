// Clasa Image

package student_andredragu_se_labs_l5ex2;

public interface Image {
	   void display();				// Metoda fara implementare din cadrul interfetei Image
}
	 
class RealImage implements Image {							// Clasa RealImage care implementeaza interfata Image
	private String fileName;
	
	public RealImage(String fileName) {						// Constructor
		this.fileName = fileName;
		loadFromDisk(fileName);
	}
	
	public void display() {									// Override la metoda display
		System.out.println("Displaying " + fileName);
	}
	
	private void loadFromDisk(String fileName) {			// Metoda loadFromDisk, metoda privata
		System.out.println("Loading " + fileName);
	}
	
}
	 
class ProxyImage implements Image {							// Clasa ProxyImage care implementeaza interfata Image
	private String fileName;
	Image [] images = new Image[3];
			 
	public ProxyImage() {									// Constructor 1
		images[0] = callImage("abc");
		images[1] = callImage("def");
		images[2] = callImage("ghi");
	}
	
	public ProxyImage(String fileName) 	{					// Constructor 2
		this.fileName = fileName;
	}

	private Image callImage(String fileName) {				// Metoda callImage
		int i = (int)(Math.random()*10);
		if(i<5)
			return new RealImage(fileName);
		else
			return new RotatedImage(fileName);
	}
	
	public void display() {									// Metoda display
		for(int i=0; i < images.length; i++)
            images[i].display();
	}
	
}

class RotatedImage implements Image {				// Clasa RotatedImage care implementeaza interfata Image
	private RealImage realImage;
	private String fileName;

	public RotatedImage(String fileName) {			// Constructor
		this.fileName = fileName;
	}

	public void display() {							// Metoda display
		if(realImage == null)
			realImage = new RealImage(fileName);
		System.out.println("Display rotated " + fileName);
	}
	
}
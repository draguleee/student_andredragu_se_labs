// Clasa TestShape

package student_andredragu_se_labs_l5ex1;

public class TestShape {

	public static void main(String[] args) {
		
		Circle ccl1 = new Circle();					// Creare cerc1
		Circle ccl2 = new Circle(2.5);				// Creare cerc2
		Circle ccl3 = new Circle(5, "blue", true);	// Creare cerc3
		
		Rectangle rct1 = new Rectangle();							// Creare dreptunghi1
		Rectangle rct2 = new Rectangle(3.5,5.5);					// Creare dreptunghi2
		Rectangle rct3 = new Rectangle(4.0, 6.0, "green", false);	// Creare dreptunghi3
			
		Square sqr1 = new Square();					// Creare patrat1
		Square sqr2 = new Square(2.0);				// Creare patrat2
		Square sqr3 = new Square(4.0, "gray", true);	// Creare patrat3
		
		double r1 = ccl1.getRadius();							// Returnare raza cerc1
		System.out.println("Raza cercului 1: " + r1);			// Afisare raza cerc1
		ccl1.setRadius(5);
		double a1 = ccl1.getArea();								// Returnare arie cerc1
		System.out.println("Aria cercului 1: " + a1); 			// Afisare arie cerc1
		double p1 = ccl1.getPerimeter(); 						// Returnare perimetru cerc1
		System.out.println("Perimetrul cercului 1: " + p1);		// Afisare perimetru cerc1
		String s1 = ccl1.toString(); 							// Metoda toString()
		System.out.println(s1);
		
		System.out.println(" ");
		
		double r2 = ccl2.getRadius(); 							// Returnare raza cerc2
		System.out.println("Raza cercului 2: " + r2); 			// Afisare raza cerc2
		ccl2.setRadius(2.5);
		double a2 = ccl2.getArea(); 							// Returnare arie cerc2
		System.out.println("Aria cercului 2: " + a2); 			// Afisare arie cerc2
		double p2 = ccl2.getPerimeter(); 						// Returnare perimetru cerc2
		System.out.println("Perimetrul cercului 2: " + p2); 	// Afisare perimetru cerc2
		String s2 = ccl2.toString(); 							// Metoda toString
		System.out.println(s2);
		
		System.out.println(" ");
		
		double r3 = ccl3.getRadius(); 							// Returnare raza cerc3
		System.out.println("Raza cercului 3: " + r3);			// Afisare raza cerc3
		ccl3.setRadius(5); 		
		double a3 = ccl3.getArea(); 							// Returnare arie cerc3
		System.out.println("Aria cercului 3: " + a3); 			// Afisare arie cerc3
		double p3 = ccl3.getPerimeter(); 						// Returnare perimetru cerc3
		System.out.println("Perimetrul cercului 3: " + p3); 	// Afisare perimetru cerc3
		String s3 = ccl3.toString(); 			 				// Metoda toString
		System.out.println(s3); 								
		
		System.out.println(" ");
		
		double w1 = rct1.getWidth(); 								// Returnare latime dreptunghi1
		System.out.println("Latimea dreptunghiului 1: " + w1);		// Afisare latime dreptunghi1
		rct1.setWidth(10);
		double l1 = rct1.getLength(); 								// Returnare lungime dreptunghi1
		System.out.println("Lungimea dreptunghiului 1: " + l1);		// Afisare lungime dreeptunghi1
		rct1.setLength(20);
		double a4 = rct1.getArea(); 								// Returnare arie dreptunghi1
		System.out.println("Aria dreptunghiului 1: " + a4);			// Afisare arie dreptunghi1
		double p4 = rct1.getPerimeter(); 							// Returnare perimetru dreptunghi1
		System.out.println("Perimetrul dreptunghiului 1: " + p4);	// Afisare perimetru dreptunghi1
		String s4 = rct1.toString();								// Metoda toString()
		System.out.println(s4);
		
		System.out.println(" "); 
		
		double w2 = rct2.getWidth();								// Returnare latime dreptunghi2
		System.out.println("Latimea dreptunghiului 2: " + w2); 		// Afisare latime dreptunghi2
		rct2.setWidth(3.5); 										
		double l2 = rct2.getLength(); 								// Returnare lungime dreptunghi2
		System.out.println("Lungimea dreptunghiului 2: " + l2); 	// Afisare lungime dreptunghi2
		rct2.setLength(5.5); 								
		double a5 = rct2.getArea();									// Returnare arie dreptunghi2
		System.out.println("Aria dreptunghiului 2: " + a5); 		// Afisare arie dreptunghi2
		double p5 = rct2.getPerimeter(); 							// Returnare perimetru dreptunghi2
		System.out.println("Perimetrul dreptunghiului 2: " + p5); 	// Afisare perimetru dreptunghi2
		String s5 = rct2.toString(); 								// Metoda toString()
		System.out.println(s5);
		
		System.out.println(" ");
		
		double w3 = rct3.getWidth(); 								// Returnare latime dreptunghi3
		System.out.println("Latimea dreptunghiului 3: " + w3); 		// Afisare latime dreptunghi3
		rct3.setWidth(4.0); 											
		double l3 = rct3.getLength(); 								// Returnare lungime dreptunghi3
		System.out.println("Lungimea dreptunghiului 3: " + l3); 	// Afisare lungime dreptunghi3
		rct3.setLength(6.0);	    								
		double a6 = rct3.getArea(); 								// Returnare arie dreptunghi3
		System.out.println("Aria dreptunghiului 3: " + a6); 		// Afisare arie dreptunghi3
		double p6 = rct3.getPerimeter(); 							// Returnare perimetru dreptunghi3
		System.out.println("Perimetrul dreptunghiului 3: " + p6); 	// Afisare perimetru dreptunghi3
		String s6 = rct3.toString(); 								// Metoda toString
		System.out.println(s6);
		
		System.out.println(" "); 
		
		double x1 = sqr1.getSide(); 							// Returnare latura patrat1
		System.out.println("Latura patratului 1: " + x1);		// Afisare latura patrat1
		sqr1.setSide(5);	
		sqr1.setWidth(5);
		sqr1.setLength(5);
		double a7 = sqr1.getArea();								// Returnare arie patrat1 	
		System.out.println("Aria patratului 1: " + a7);			// Afisare arie patrat1
		double p7 = sqr1.getPerimeter();						// Returnare perimetru patrat1
		System.out.println("Perimetrul patratului 1: " + p7);	// Afisare perimetru patrat1
		String s7 = sqr1.toString();							// Metoda toString()
		System.out.println(s7);
		
		System.out.println(" ");
		
		double x2 = sqr2.getSide(); 							// Returnare latura patrat2
		System.out.println("Latura patratului 2: " + x2); 		// Afisare latura patrat2
		sqr2.setSide(2);
		sqr2.setWidth(2);
		sqr2.setLength(2);
		double a8 = sqr2.getArea(); 							// Returnare arie patrat2
		System.out.println("Aria patratului 2: " + a8);			// Afisare arie patrat2
		double p8 = sqr2.getPerimeter(); 						// Returnare perimetru patrat2
		System.out.println("Perimetrul patratului 2: " + p8); 	// Afisare perimetru patrat2
		String s8 = sqr2.toString(); 							// Metoda toString()
		System.out.println(s8);
		
		System.out.println(" ");
		
		double x3 = sqr3.getSide(); 							// Returnare latura patrat3
		System.out.println("Latura patratului 3: " + x3); 		// Afisare latura patrat3
		sqr3.setSide(4);
		sqr3.setWidth(4);
		sqr3.setLength(4);
		double a9 = sqr3.getArea(); 							// Returnare arie patrat3
		System.out.println("Aria patratului 3: " + a9); 		// Afisare arie patrat3
		double p9 = sqr3.getPerimeter(); 						// Returnare perimetru patrat3
		System.out.println("Perimetrul patratului 3: " + p9); 	// Afisare perimetru patrat3
		String s9 = sqr3.toString(); 							// Metoda toString()
		System.out.println(s9);
		
	}
}

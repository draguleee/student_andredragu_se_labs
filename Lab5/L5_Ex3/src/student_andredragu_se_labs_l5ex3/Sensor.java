// Clasa Sensor

package student_andredragu_se_labs_l5ex3;

import java.util.Random;
import java.util.concurrent.TimeUnit;

abstract class Sensor {								// Superclasa Sensor
	private String location;
	
	public abstract int readValue();				// Metoda abstracta
	
	public String getLocation() {					// Getter pentru locatie
		return this.location;
	}
	
}

class TemperatureSensor extends Sensor { 			// Subclasa TemperatureSensor extinde superclasa Sensor
	
	Random tempSensor = new Random();
	
	public int readValue() {						// Returnare valoare senzor temperatura
		return tempSensor.nextInt(100);
	}
}

class LightSensor extends Sensor { 					// Subclasa LightSensor extinde superclasa Sensor
	
	Random lightSensor = new Random();
	
	public int readValue() {						// Returnare valoare senzor lumina
		return lightSensor.nextInt(100);
	}
	
}



package student_andredragu_se_labs_l5ex3;

import java.util.concurrent.TimeUnit;

class Controller {													// Clasa Controller
	public TemperatureSensor t = new TemperatureSensor();
	public LightSensor l = new LightSensor();
	
	public void control() throws InterruptedException {							// Metoda Control
		for(int i = 1; i <=20 ; i++) {
			System.out.println("Temperatura: " + t.readValue());				// Afisare valoare temperatura
			System.out.println(" ");
			System.out.println("Intensitatea luminii: " + l.readValue());		// Afisare valoare intensitate luminoasa
			TimeUnit.SECONDS.sleep(1);
		}
	}
	
	public static void main(String[] args) throws InterruptedException {		// Metoda main
		Controller c = new Controller();
		c.control();
	}
}

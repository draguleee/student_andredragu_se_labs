// Clasa TestBook

package student_andredragu_se_labs_l4ex4;

public class TestBook {

		public static void main(String[] args) {
			
			Author[] a = new Author[2];			// Vor exista 4 autori
			a[0] = new Author("Markus Zusak", "markus_zusak@gmail.com", 'm');					// Creare primul autor
			a[1] = new Author("Giovanni Boccaccio", "giovanni.boccaccio@yahoo.com", 'm');		// Creare al doilea autor
			
			Book b1 = new Book("Hotul de Carti", a, 35);
			Book b2 = new Book("Decameronul", a, 190, 50);
			
			String name1 = b1.getName(); 				// b1.getName - returneaza numele primei carti
			System.out.println("Numele primei carti: " + name1);					// Afisarea numelui primei carti
			
			System.out.println(" ");
			
			String name2 = b2.getName(); 				// b2.getName - returneaza numele celei de-a doua carte
			System.out.println("Numele celei de-a doua carte: " + name2);					// Afisarea numelui celei de-a doua carte
			
			System.out.println(" ");
			
			b1.getAuthors();							// Returneaza numele autorilor primei carti
			b2.getAuthors();							// Returneaza numele autorilor celei de-a doua carte
			
			System.out.println(" ");
			
			double price1 = b1.getPrice();									// b1.getPrice - returneaza pretul primei carti
			System.out.println("Pretul primei carti: " + price1);			// Afisarea pretului primei carti
			double price2 = b2.getPrice();									// b2.getPrice - returneaza pretul celei de-a doua carte
			System.out.println("Pretul celei de-a doua carte: " + price2);	// Afisarea pretului celei de-a doua carte
			
			System.out.println(" ");
			
			int quan1 = b1.getQtyInStock(); 			// b1.getQtyInStock - returneaza numarul de exemplare disponibil prima carte
			System.out.println("Stoc prima carte: " + quan1);		// Afisarea stocului primei carti
			int quan2 = b2.getQtyInStock();				// b2.getQtyInStock - returnewza numarul de exemplare disponibil a doua carte
			System.out.println("Stoc a doua carte: " + quan2);		// Afisarea stocului celei de-a doua carte
			
			System.out.println(" ");
			
			b1.setPrice(25);
			b1.setQtyInStock(100);
			b2.setPrice(55);
			b2.setQtyInStock(85);
			
			b1.printAuthors();				// Afisare autori
			
			System.out.println(" ");
	
			b1.toString();
			b2.toString();	
		}
		
}

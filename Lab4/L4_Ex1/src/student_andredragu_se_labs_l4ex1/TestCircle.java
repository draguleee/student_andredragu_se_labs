// Clasa TestCircle

package student_andredragu_se_labs_l4ex1;

public class TestCircle {
	
	public static void main(String[] args) {
		
		Circle c1 = new Circle();		// Crearea unui nou cerc cu primul constructor
		Circle c2 = new Circle(10, "blue");		// Crearea unui nou cerc cu al doilea constructor
		
		double raza1 = c1.getRadius();									// Returnarea razei pentru primul cerc
		System.out.println("Raza cercului 1 este: " + raza1);			// Afisare raza primul cerc
		double aria1 = c1.getArea();									// Returnarea ariei pentru primul cerc
		System.out.println("Aria cercului 1 este: " + aria1);			// Afisare arie primul cerc
		
		System.out.println(" ");
		
		double raza2 = c2.getRadius(); 							// Returnarea razei pentru al doilea cerc		
		System.out.println("Raza cercului 2 este: " + raza2);	// Afisare raza al doilea cerc
		double aria2 = c2.getArea(); 							// Returnarea ariei pentru al doilea cerc
		System.out.println("Aria cercului 2 este: " + aria2);	// Afisare arie al doilea cerc
	}
	
}

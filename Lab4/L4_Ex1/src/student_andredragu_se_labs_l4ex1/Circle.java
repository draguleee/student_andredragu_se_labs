// Clasa Circle

package student_andredragu_se_labs_l4ex1;

public class Circle {
	
	private double radius = 1.0;		// Raza cercului
	private String color = "red";		// Culoarea cercului
	
	Circle() { 										// Constructor suprainscris
		this.radius = 1.0;
		this.color = "red";
	}
	
	Circle(double r, String c) {					// Constructor suprainscris
		this.radius = r;
		this.color = c;
	}
	
	public double getRadius() {			// Getter pentru raza cercului
		return this.radius;
	}
	
	public double getArea() {			// Getter pentru aria cercului
		return Math.PI * this.radius * this.radius;
	}
	
}

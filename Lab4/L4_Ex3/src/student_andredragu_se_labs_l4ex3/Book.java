// Clasa Book

package student_andredragu_se_labs_l4ex3;

public class Book {
	private String name;		// Numele cartii
	private Author author;		// Autorul cartii
	private double price;		// Pretul cartii
	private int qtyInStock = 0;	// Numarul de carti in stoc
	
	public Book(String n, Author a, double p) {				// Constructor
		this.name = n;
		this.author = a;
		this.price = p;
	} 
	
	public Book(String n, Author a, double p, int qis) {	// Constructor
		this.name = n;
		this.author = a;
		this.price = p;
		this.qtyInStock = qis;
	}
	
	public String getName() {					// Getter pentru numele cartii
		return this.name;
	}
	
	public Author getAuthor() {					// Getter pentru autorul cartii
		return this.author;
	}
	
	public double getPrice() { 					// Getter pentru pretul cartii
		return this.price;
	}
	
	public void setPrice(double x) { 			// Setter pentru pretul cartii
		this.price = x;
	}
	
	public int getQtyInStock() {				//  Getter pentru numarul de exemplare in stoc
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int y) { 			// Setter pentru numarul de exemplare in stoc
		this.qtyInStock = y;
	}
	
	public String ToString() {					// Metoda ToString
		return "Carte:" + " '" + this.name + "' " + " by " + this.author;
	}
	
}
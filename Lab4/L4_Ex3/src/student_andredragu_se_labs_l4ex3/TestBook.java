// Clasa TestBook

package student_andredragu_se_labs_l4ex3;

public class TestBook {
	
	public static void main(String[] args) {
		
		Author a1 = new Author("Exupery", " exupery_antoine@gmail.com ", 'm');			// Creare autor1
		Author a2 = new Author("Fitzgerald", " fitzgerald@gmail.com ", 'm');			// Creare autor2
		
		Book b1 = new Book("Micul Print" , a1, 15);				// Creare carte
		Book b2 = new Book("Marele Gatsby" , a2 , 25, 150);		// Creare carte
		
		String aname1 = a1.getName();								// Returnare nume primul autor
		System.out.println("Numele autorului1: " + aname1);			// Afisare nume primul autor
		String email1 = a1.getEmail();								// Returnare email primul autor
		System.out.println("E-mailul autorului1 este: " + email1 ); // Afisare email primul autor
		a1.setEmail(" exupery_antoine@gmail.com ");		
		char gender1 = a1.getGender();								// Returnare sex primul autor
		System.out.println("Sexul autorului1: " + gender1); 		// Adisare sex primul autor
		
		System.out.println(" ");
		
		String aname2 = a2.getName();								// Returnare nume al doilea autor
		System.out.println("Numele autorului2: " + aname2); 		// Afisare nume al doilea autor
		String email2 = a2.getEmail();								// Returnare email al doilea autor
		System.out.println("E-mailul autorului2 este: " + email2); 	// Afisare email al doilea autor	
		a2.setEmail(" fitzgerald@gmail.com ");		
		char gender2 = a2.getGender();								// Returnare sex al doilea autor
		System.out.println("Sexul autorului2: " + gender2); 		// Afisare sexl al doilea autor
		
		System.out.println(" ");
		
		String bname1 = b1.getName();								// Returnare nume prima carte
		System.out.println("Numele cartii1: " + bname1); 			// Afisare nume prima carte
		Author aut1 = b1.getAuthor();								// Returnare autor prima carte
		System.out.println("Autorul cartii1: " + aut1); 			// Afisare autor prima carte
		double p1 = b1.getPrice();									// Returnare pret prima carte
		System.out.println("Pretul cartii1: " + p1); 				// Afisare pret prima carte
		b1.setPrice(15);
		int q1 = b1.getQtyInStock();								// Returnare stoc prima carte
		System.out.println("Stoc carte1: " + q1); 					// Afisare stoc prima carte
		b1.setQtyInStock(10);
		String s1 = b1.ToString();									// Metoda toString
		System.out.println(s1);										// Afisare
		
		System.out.println(" ");
		
		String bname2 = b2.getName();								// Returnare nume a doua carte
		System.out.println("Numele cartii2: " + bname2); 			// Afisare nume a doua carte
		Author aut2 = b2.getAuthor();								// Returnare autor a doua carte
		System.out.println("Autorul cartii2: " + aut2);				// Afisare autor a doua carte
		double p2 = b2.getPrice();									// Returnare pret a doua carte
		System.out.println("Pretul cartii2: " + p2); 				// Afisare pret a doua carte
		b2.setPrice(20);
		int q2 = b2.getQtyInStock(); 								// Returnare stoc a doua carte
		System.out.println("Stoc carte2: " + q2); 					// Afisare stoc a doua carte
		b2.setQtyInStock(15);
		String s2 = b2.ToString(); 									// Metoda toString()
		System.out.println(s2); 									// Afisare
	}
	
}

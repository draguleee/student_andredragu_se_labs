// Clasa Author

package student_andredragu_se_labs_l4ex3;

public class Author {
	
	private String name;		// Nume autor
	private String email;		// E-mail autor
	private char gender;		// Sex autor
	
	public Author(String n, String e, char g) {		// Constructor cu this keyword
		this.name = n;
		this.email = e;
		this.gender = g;
    }
	
	public String getName() {					// Getter pentru numele autorului
		return this.name;
	}
	
	public String getEmail() {					// Getter pentru emailul autorului
		return this.email;
	}
		
	public void setEmail(String email) {		// Setter pentru email
		this.email = email;
	}
	
	public char getGender() {					// Getter pentru sexul autorului
		return this.gender;
	}
	
	public String toString () {					// Metoda toString
		return " ' " + this.name + " ' " + "(" + this.gender + ")" + " at" + this.email ;
		
	}
	
}
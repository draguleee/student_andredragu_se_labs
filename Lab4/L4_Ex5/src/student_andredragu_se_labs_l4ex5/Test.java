// Clasa Test

package student_andredragu_se_labs_l4ex5;

public class Test {

	public static void main(String[] args) {
		
		Cylinder q1 = new Cylinder();				// Creare cilindru1
		Cylinder q2 = new Cylinder(3.0, "black");	// Creare cilindru2
		Cylinder q3 = new Cylinder(1.5, "red", 10);	// Creare cilindru3
		
		double h1 = q1.getHeight(); 		// Returnare inaltime cilindru1
		System.out.println("Inaltime cilindru1: " + h1);		// Afisare inaltime cilindru1
		double a1 = q1.getArea();			// Returnare arie cilindru1
		System.out.println("Arie cilindru1: " + a1);			// Afisare arie cilindru1
		double v1 = q1.getVolume(); 		// Returnare volum cilindru1
		System.out.println("Volum cilindru1: " + v1);
		
		System.out.println(" ");
		
		double h2 = q2.getHeight(); 		// Returnare inaltime cilindru2
		System.out.println("Inaltime cilindru2: " + h2); 		// Afisare inaltime cilindru2
		double a2 = q2.getArea(); 			// Returnare arie cilindru2
		System.out.println("Arie cilindru2: " + a2); 			// Afisare arie cilindru2
		double v2 = q2.getVolume(); 		// Returnare volum cilindru2
		System.out.println("Volum cilindru2: " + v2); 			// Afisare volum cilindru2
		
		System.out.println(" ");
		
		double h3 = q3.getHeight(); 		// Returnare inaltime cilindru3
		System.out.println("Inaltime cilindru3: " + h3);		// Afisare inaltime cilindru3
		double a3 = q3.getArea(); 			// Returnare arie cilindru3
		System.out.println("Arie cilindru3: " + a3); 			// Afisare arie cilindru3
		double v3 = q3.getVolume();			// Returnare volum cilindru3
		System.out.println("Volum cilindru3: " + v3); 			// Afisare volum cilindru3
		
	}
	
}

// Clasa Circle

package student_andredragu_se_labs_l4ex5;

public class Circle {			// Superclasa
	
	private double radius = 1.0;		// Raza cercului
	private String color = "red";		// Culoarea cercului
	
	Circle() { 							// Constructor suprainscris
		this.radius = 1.0;	
		this.color = "red";
	}
	
	Circle(double r, String c) {		// Constructor suprainscris
		this.radius = r;
		this.color = c;
	}
	
	public double getRadius() {			// Getter pentru raza cercului
		return this.radius;
	}
	
	public double getArea() {			// Getter pentru aria cercului
		return Math.PI * this.radius * this.radius;
	}
	
}

class Cylinder extends Circle { 		// Subclasa
	private double height = 1.0;
	
	Cylinder() {			// Primul constructor
		super();		
		this.height = 1.0;
	}
	
	Cylinder(double r, String c) { 	// Al doilea constructor
		super(r,c);
	}
	
	Cylinder(double r, String c, double h) { 	// Al treilea constructor
		super(r,c);
		this.height = h;
	}
	
	public double getHeight() {			// Getter pentru inaltimea cilindrului
		return this.height;
	}
	
	public double getArea() { 			// Getter pentru aria cilindrului
		return 2 * Math.PI * this.getRadius() * (this.getRadius() + this.height);
	}
	
	public double getVolume() {			// Getter pentru volumul cilindrului
		return Math.PI * this.getRadius() * this.getRadius() * this.height;
	}
	
}
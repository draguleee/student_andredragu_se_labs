// Clasa TestAuthor

package student_andredragu_se_labs_l4ex2;

public class TestAuthor {
		
	public static void main(String[] args) {
		
		Author a1 = new Author("Ana" , "ana_1999@gmail.com", 'f');		// Creare autor nou
		Author a2 = new Author("Ion" , "ion_1997@gmail.com", 'm');		// Creare autor nou
		
		String nume1 = a1.getName();								// Returnare nume primul autor
		System.out.println("Numele primului autor: " + nume1);		// Afisare nume primul autor	
		String email1 = a1.getEmail();								// Returnare email primul autor
		System.out.println("E-mailul primului autor: " + email1); 	// Afisare email primul autor
		a1.setEmail("ana_1999@gmail.com");		
		char gender1 = a1.getGender();							 	// Returnare sex primul autor
		System.out.println("Sexul primului autor: " + gender1);		// Afisare sex primul autor
		String string1 = a1.toString();								// Metoda toString
		System.out.println(string1);								// Afisare
		
		System.out.println(" ");
		
		String nume2 = a2.getName();										// Returnare nume al doilea autor
		System.out.println("Numele celui de-al doilea autor: " + nume2); 	// Afisare nume al doilea autor
		String email2 = a2.getEmail(); 										// Returnare email al doilea autor
		System.out.println("E-mailul celui de-al doilea autor: " + email2);	// Afisare email al doilea autor
		a2.setEmail("ion_1997@gmail.com");		
		char gender2 = a2.getGender();										// Returnare sex al doilea autor
		System.out.println("Sexul celui de-al doilea autor: " + gender2);	// Afisare sex al doilea autor
		String string2 = a2.toString();										// Metoda toString
		System.out.println(string2);										// Afisare
	}

}
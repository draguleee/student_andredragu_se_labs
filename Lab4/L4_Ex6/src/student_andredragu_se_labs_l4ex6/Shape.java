// Clasa Shape

package student_andredragu_se_labs_l4ex6;

public class Shape {			// Superclasa Shape
	private String color = "red";		// Culoarea figurii
	private boolean filled = true;		// Variabila pentru a determina daca figura este sau nu umpluta cu culoare
	
	public Shape() { 		// Constructor 1
		this.color = "red";
		this.filled = true;
	}
	
	public Shape(String c, boolean f) {		// Constructor 2
		this.color = c;
		this.filled = f;
	}
	
	public String getColor() { 				// Getter pentru culoarea formei
		return this.color;
	}
	
	public void setColor(String c) {	 	// Setter pentru culoarea formei
		this.color = c;
	}
	
	public boolean isFilled() { 			// Getter pentru filled
		return filled;
	}
	
	public void setFilled(boolean f) { 		// Setter pentru filled
		this.filled = f;
	}
	
	public String toString() {				// Metoda toString()
		String f;
		if(this.filled == true)
			f = "filled!";
		else
			f = "not filled!";
		String output =   " A Shape with color of " + this.color + " and " + f; 
		return output;
	}
	
}

class Circle extends Shape {			// Subclasa Circle
	private double radius = 1.0;		// Raza cercului
	
	public Circle() {			// Constructor 1
		super();
		this.radius = 1.0;
	}
	
	public Circle(double r) {			// Constructor 2	
		super();
		this.radius = r;
	}
	
	public Circle(double r, String c, boolean f) { 			// Constructor 3
		super(c,f);
		this.radius = r;
	}
	
	public double getRadius() { 		// Getter pentru raza cercului
		return this.radius;
	}
	
	public void setRadius(double r) { 	// Setter pentru raza cercului
		this.radius = r;
	}
	
	public double getArea() { 			// Getter pentru aria cercului
		return Math.PI * this.radius * this.radius;
	}
	
	public double getPerimeter() { 		// Getter pentru perimetrul cercului
		return 2 * Math.PI * this.radius;
	}
	
	public String toString() { 			// Metoda toString()
		String output = "A Circle with radius = " + this.radius + ", which is a subclass of" + super.toString();
		return  output;
	}
	
}	

class Rectangle extends Shape { 		// Subclasa Rectangle
	private double width = 1.0;
	private double length = 1.0;
	
	public Rectangle() {			// Constructor 1
		super();
		this.width = 1.0;
		this.length = 3.0;
	}
	
	public Rectangle(double w, double l) {			// Constructor 2
		super();
		this.width = w;
		this.length = l;
	}
	
	public Rectangle(double w, double l, String c, boolean f) { 			// Constructor 3
		super(c,f);
		this.width = w;
		this.length = l;
	}
	
	public double getWidth() { 			// Getter pentru latimea dreptunghiului
		return this.width;
	}
	
	public void setWidth(double w) { 	// Setter pentru latimea dreptunghiului
		this.width = w;
	}
	
	public double getLength() { 		// Getter pentru lungimea dreptunghiului
		return this.length;
	}
	
	public void setLength(double l) { 	// Setter pentru lungimea dreptunghiului
		this.length = l;
	}
	
	public double getArea() { 			// Getter pentru aria dreptunghiului
		return this.width * this.length;
	}
	
	public double getPerimeter() { 		// Getter pentru perimetrul dreptunghiului
		return 2 * this.width + 2 * this.length; 
	}
	
	public String toString() { 			// Metoda toString()
		String output =  "A Rectangle with width = " + this.width + " and length = " + this.length + " which is a subclass of " + super.toString();
		return output;
	}
}

class Square extends Rectangle { 		// Subclasa Square
	
	public Square() { 			// Constructor 1
		super();
	}
	
	public Square(double s) { 			// Constructor 2
		super();
		super.setWidth(s);
		super.setLength(s);
	}
	
	public Square(double s, String c, boolean f) {			// Constructor 3 
		super(s,s,c,f);
	}
	
	public double getSide() {			// Getter pentru latura patratului
		return super.getWidth();
	}
	
	public void setSide(double s) { 	// Setter pentru latura patratului
		super.setWidth(s);
		super.setLength(s);
	}
	
	public void setWidth(double s) { 	// Setter pentru latime
		super.setWidth(s);
	}
	
	public void setLength(double s) { 	// Setter pentru lungime
		super.setLength(s);
	}
	
	public double getArea() { 			// Getter pentru aria patratului
		return this.getSide() * this.getSide();
	}
	
	public double getPerimeter() { 		// Getter pentru perimetrul patratului
		return 4 * this.getSide();
	}
	public String toString() { 			// Metoda toString()
		String output = "A Square with side = " + this.getSide() + " which is a subclass of " + super.toString();
		return output;
	}
	
}
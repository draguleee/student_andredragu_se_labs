// Clasa TestShape

package student_andredragu_se_labs_l4ex6;

public class TestShape {
	
	public static void main(String[] args) {
		
		Circle circle1 = new Circle();
		Circle circle2 = new Circle(2.0);
		Circle circle3 = new Circle(1.5, "blue", false);
		
		Rectangle rectangle1 = new Rectangle();
		Rectangle rectangle2 = new Rectangle(5, 10);
		Rectangle rectangle3 = new Rectangle(2, 6, "brown", true);
		
		Square square1 = new Square();
		Square square2 = new Square(5);
		Square square3 = new Square(8, "yellow", false);
		
		double r1 = circle1.getRadius(); 							// Returnare raza cerc1
		System.out.println("Raza cercului 1: " + r1);				// Afisare raza cerc1
		circle1.setRadius(1.0);
		double a1 = circle1.getArea(); 								// Returnare arie cerc1
		System.out.println("Aria cercului 1: " + a1);				// Afisare arie cerc1
		double p1 = circle1.getPerimeter(); 						// Returnare perimetru cerc1
		System.out.println("Perimetrul cercului 1: " + p1);			// Afisare perimetru cerc1
		String s1 = circle1.toString();								// Metoda toString()
		System.out.println(s1);
		
		System.out.println(" ");
		
		double r2 = circle2.getRadius(); 							// Returnare raza cerc2
		System.out.println("Raza cercului 2: " + r2); 				// Afisare raza cerc2
		circle2.setRadius(2.0);
		double a2 = circle2.getArea(); 								// Returnare arie cerc2
		System.out.println("Aria cercului 2: " + a2);				// Afisare arie cerc2
		double p2 = circle2.getPerimeter(); 						// Returnare perimetru cerc2
		System.out.println("Perimetrul cercului 2: " + p2); 		// Afisare perimetru cerc2
		String s2 = circle2.toString(); 							// Metoda toString()
		System.out.println(s2);
		
		System.out.println(" ");
		
		double r3 = circle3.getRadius(); 							// Returnare raza cerc3
		System.out.println("Raza cercului 3: " + r3); 				// Afisare raza cerc3
		circle3.setRadius(1.5);
		double a3 = circle3.getArea(); 								// Returnare arie cerc3
		System.out.println("Aria cercului 3: " + a3);				// Afisare arie cerc3
		double p3 = circle3.getPerimeter(); 						// Returnare perimetru cerc3
		System.out.println("Perimetrul cercului 3: " + p3); 		// Afisare perimetru cerc3
		String s3 = circle3.toString(); 							// Metoda toString()
		System.out.println(s3);
		
		System.out.println(" ");
		
		double w1 = rectangle1.getWidth(); 							// Returnare latime dreptunghi1
		System.out.println("Latimea dreptunghiului 1: " + w1);		// Afisare latime dreptunghi1
		rectangle1.setWidth(10);
		double l1 = rectangle1.getLength(); 						// Returnare lungime dreptunghi1
		System.out.println("Lungimea dreptunghiului 1: " + l1); 	// Afisare lungime dreptunghi1
		rectangle1.setLength(20);
		double a4 = rectangle1.getArea(); 							// Returnare arie dreptunghi1
		System.out.println("Aria dreptunghiului 1: " + a4); 		// Afisare arie dreptunghi1
		double p4 = rectangle1.getPerimeter(); 						// Returnare perimetru dreptunghi1
		System.out.println("Perimetrul dreptunghiului 1: " + p4); 	// Afisare perimetru dreptunghi1
		String s4 = rectangle1.toString(); 							// Metoda toString()
		System.out.println(s4); 									
		
		System.out.println(" ");
		
		double w2 = rectangle2.getWidth(); 							// Returnare latime dreptunghi2
		System.out.println("Latimea dreptunghiului 2: " + w2); 		// Afisare latime dreptunghi2
		rectangle2.setWidth(5);
		double l2 = rectangle2.getLength();							// Returnare lungime dreptunghi2
		System.out.println("Lungimea dreptunghiului 2: " + l2); 	// Afisare lungime dreptunghi2
		rectangle2.setLength(10); 							
		double a5 = rectangle2.getArea(); 							// Returnare arie dreptunghi2
		System.out.println("Aria dreptunghiului 2: " + a5); 		// Afisare arie dreptunghi2
		double p5 = rectangle2.getPerimeter(); 						// Returnare perimetru dreptunghi2
		System.out.println("Perimetrul dreptunghiului 2: " + p5); 	// Afisare perimetru dreptunghi2
		String s5 = rectangle2.toString(); 							// Metoda toString()
		System.out.println(s5);
		
		System.out.println(" ");
		
		double w3 = rectangle3.getWidth(); 							// Returnare latime dreptunghi3
		System.out.println("Latimea dreptunghiului 3: " + w3); 		// Afisare latime dreptunghi3
		rectangle3.setWidth(2);
		double l3 = rectangle3.getWidth(); 							// Returnare lungime dreptunghi3
		System.out.println("Lungimea dreptunghiului 3: " + l3); 	// Afisare lungime dreptunghi3
		rectangle3.setLength(6);									
		double a6 = rectangle3. getArea(); 							// Returnare arie dreptunghi3
		System.out.println("Aria dreptunghiului 3: " + a6);			// Afisare arie dreptunghi3
		double p6 = rectangle3.getPerimeter(); 						// Returnare perimetru dreptunghi3
		System.out.println("Perimetrul dreptunghiului 3: " + p6); 	// Afisare perimetru dreptunghi3
		String s6 = rectangle3.toString(); 							// Metoda toString()
		System.out.println(s6);
		
		System.out.println(" ");
		
		double x1 = square1.getSide(); 								// Returnare latura patrat1
		System.out.println("Latura patratului 1: " + x1); 			// Afisare latura patrat1
		square1.setSide(1);
		square1.setWidth(1);
		square1.setLength(1);
		double a7 = square1.getArea(); 								// Returnare arie patrat1
		System.out.println("Aria patratului 1: " + a7); 			// Afisare arie patrat1
		double p7 = square1.getPerimeter(); 						// Returnare perimetru patrat1
		System.out.println("Perimetrul patratului 1: " + p7); 		// Afisare perimetru patrat1
		String s7 = square1.toString(); 							// Metoda toString()
		System.out.println(s7);
		
		System.out.println(" ");
		
		double x2 = square2.getSide(); 								// Returnare latura patrat2
		System.out.println("Latura patratului 2: " + x2); 			// Afisare latura patrat2
		square1.setSide(5);
		square1.setWidth(5);;
		square1.setLength(5);
		double a8 = square2.getArea(); 								// Returnare arie patrat2
		System.out.println("Aria patratului 2: " + a8); 			// Afisare arie patrat2
		double p8 = square2.getPerimeter(); 						// Returnare perimetru patrat2
		System.out.println("Perimetrul patratului 1: " + p8); 		// Afisare perimetru patrat2
		String s8 = square2.toString(); 							// Metoda toString()
		System.out.println(s8);
		
		System.out.println(" ");
		
		double x3 = square3.getSide(); 								// Returnare latura patrat3
		System.out.println("Latura patratului 3: " + x3); 			// Afisare latura patrat3
		square1.setSide(8);
		square1.setWidth(8);;
		square1.setLength(8);
		double a9 = square3.getArea(); 								// Returnare arie patrat3
		System.out.println("Aria patratului 3: " + a9); 			// Afisare arie patrat3
		double p9 = square3.getPerimeter(); 						// Returnare perimetru patrat3
		System.out.println("Perimetrul patratului 3: " + p9); 		// Afisare perimetru patrat3
		String s9 = square3.toString(); 							// Metoda toString()
		System.out.println(s9);
		
	}

}

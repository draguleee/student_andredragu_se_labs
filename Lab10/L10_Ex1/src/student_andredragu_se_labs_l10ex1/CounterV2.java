// Clasa CounterV2
// In main, in loc de metoda start(), vom avea metoda run()
// Observam counterele ca incep sa numere pe rand pana la 20
// Cand primul counter a terminat de numarat, va incepe sa numere al doilea counter si asa mai departe

package student_andredragu_se_labs_l10ex1;

public class CounterV2 extends Thread {
	 
    CounterV2(String name) {
          super(name);
    }

    public void run() {
          for(int i=0;i<20;i++) {
                System.out.println(getName() + " i = "+i);
                try {
                      Thread.sleep((int)(Math.random() * 1000));
                } 
                catch (InterruptedException e) {
                      e.printStackTrace();
                }
          }
          System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
          Counter c1 = new Counter("counter1");						// Creare counter c1
          Counter c2 = new Counter("counter2");						// Creare counter c2
          Counter c3 = new Counter("counter3");						// Creare counter c3

          c1.run();						// Pornire counter c1
          c2.run();						// Pornire counter c2
          c3.run();						// Pornire counter c3
          
          // Se observa ca, counterele incep sa numere, pe rand, pana la 20.
          // Cand un counter a terminat de numarat, va incepe urmatorul sa numere.
    }
}

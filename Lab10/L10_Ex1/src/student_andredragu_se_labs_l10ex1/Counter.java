// Clasa Counter
// In main, se va apela functia start()
// Observam counterele ca incep sa numere, in paralel, pana la 20

package student_andredragu_se_labs_l10ex1;

public class Counter extends Thread {			// Clasa Counter extinde clasa Thread
	 
	Counter(String name) {				// Constructor pentru counter, care primeste ca parametru numele counterului
		super(name);					// Mostenire nume din clasa Thread
    }

    public void run() {					// Metoda run()		
          for(int i=0; i<20; i++) {									// Parcurgere contor i (pana la cat va numara counterul)
                System.out.println(getName() + " i = "+i);			// 
                try {
                      Thread.sleep((int)(Math.random() * 1000));
                } 
                catch (InterruptedException e) {
                      e.printStackTrace();
                }
          }
          System.out.println(getName() + " job finalised.");
    }

    public static void main(String[] args) {
    	
          Counter c1 = new Counter("counter1");					// Creare counter c1
          Counter c2 = new Counter("counter2");					// Creare counter c2
          Counter c3 = new Counter("counter3");					// Creare counter c3

          c1.start();				// Start counter c1
          c2.start();				// Start counter c2
          c3.start();				// Start counter c3
          
          // Se observa ca, counterele incep sa numere deodata (in paralel), pana la 20.
    }
}
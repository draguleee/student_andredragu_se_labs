// Exemplul 2, Laboratorul 5, sectiunea Clase Abstracte si Interfete
// Exemplu clasa GraphicObject
// Implementarea unei clase abstracte

package student_andredragu_se_labs_l5exemplul2;

abstract class GraphicObject {
    int x, y;    
    
    void moveTo(int newX, int newY) {	 //metoda normala
    	System.out.println("Move graphic object to position" + x + ": " + y);
    }
    
    abstract void draw();                //metoda abstracta
}     

class Circle extends GraphicObject {
	
    void draw() {
        System.out.println("Draw circle");
    }
}

class Rectangle extends GraphicObject {
	
    void draw() {
        System.out.println("Draw rectangle");
    }
}
// Exemplul 1, Laboratorul 5, sectiunea Polimorfismul
// Exemplu clasa BirdController

package student_andredragu_se_labs_l5exemplul1;

class Bird {
	
    public void move() {
    	System.out.println("The bird is moving.");
    }
}

class Penguin extends Bird {
	
    public void move() {
    	System.out.println("The PENGUIN is swiming.");
    }
}

class Goose extends Bird {
	
    public void move() {
    	System.out.println("The GOOSE is flying.");
    }
}

public class BirdController {
    Bird[] birds = new Bird[3];
    
    BirdController() {
    	birds[0] = createBird();
    	birds[1] = createBird();
    	birds[2] = createBird();
    }
    
    public void relocateBirds() {
    	for(int i = 0; i < birds.length; i++)
    		birds[i].move();
    }

    private Bird createBird() {
    	int i = (int)(Math.random()*10);
    	if(i<5)
    		return new Penguin();
    	else
    		return new Goose();
    }

    public static void main(String [] args) {
    	BirdController bc = new BirdController();
    	bc.relocateBirds();
    }          
}
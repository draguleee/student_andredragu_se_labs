// Exemplul 6, Laboratorul 5, sectiunea Diagrame UML
// Exemplu clasa Employee
// Implementare clasa simpla cu atribute si metode statice

package student_andredragu_se_labs_l5exemplul6;

public class Employee {
    private static String department = "R&D";
    private int empId;
    
    private Employee(int employeeId) {
        this.empId = employeeId;
    }
    
    public static String getEmployee(int emplId) {
        if (emplId == 1) {
            return "Sample employee";
        } 
        else {
            return "Employee not found";
        }
    }
    
    public static String getDepartment() {
        return department;
    }
}
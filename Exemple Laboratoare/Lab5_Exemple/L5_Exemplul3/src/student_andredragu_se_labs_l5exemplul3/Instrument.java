// Exemplul 3, Laboratorul 5, sectiunea Clase Abstracte si Interfete
// Exemplu clasa Instrument
// Implementarea unei interfete

package student_andredragu_se_labs_l5exemplul3;

interface Instrument {

    void play();		 // Defineste o metoda fara implementare
}

class Pian implements Instrument {		// Clasa care implementeaza interfata
 
    public void play() {				// Trebuie obligatoriu sa implementeze metoda play()
    	System.out.println("Pian.play()");
    }
}

class Vioara implements Instrument {	// Clasa care implementeaza interfata
    
	public void play() {				// Trebuie obligatoriu sa implementeze metoda play()
		System.out.println("Vioara.play()");
    }
}

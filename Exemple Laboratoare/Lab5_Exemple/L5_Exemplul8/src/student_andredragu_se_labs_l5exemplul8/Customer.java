// Exemplul 8, Laboratorul 5, sectiunea Diagrame UML
// Exemplu clasa Customer
// Implementare clasa asociere bidirectionala

package student_andredragu_se_labs_l5exemplul8;

public class Customer {
	private String name;
	private String address;
	private String contactNumber;
	private Car car;
}
 
class Car {
    private String modelNumber;
    private Customer owner;
}
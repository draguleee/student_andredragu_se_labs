// Exemplul 7, Laboratorul 5, sectiunea Diagrame UML
// Exemplu clasa Customer
// Implementare clase cu asociere unidirectionala

package student_andredragu_se_labs_l5exemplul7;

public class Customer {
	private String name;
	private String address;
	private String contactNumber;
}
 
class Car {
    private String modelNumber;
    private Customer owner;
}
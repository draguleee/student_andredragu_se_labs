// Exemplul 5, Laboratorul 5, sectiunea Diagrame UML
// Exemplu clasa Car
// Implementarea unei clase avand la dispozitie diagrama sa UML

package student_andredragu_se_labs_l5exemplul5;

public class Car {
    private String carColor;
    private double carPrice = 0.0;
    
    public String getCarColor(String model) {
        return carColor;
    }
 
    public double getCarPrice(String model) {
        return carPrice;
    }
}
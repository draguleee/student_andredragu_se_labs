// Exemplul 4, Laboratorul 5, sectiunea Clase Abstracte si Interfete
// Exemplu clasa Instrument
// Implementare interfata cu fire de executie

package student_andredragu_se_labs_l5exemplul4;

import java.util.Random;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
 
class SoundControler {
 
    public static void playSound(final String note) {
    	
        try {
        	Clip clip = AudioSystem.getClip();
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(Muzica.class.getResourceAsStream(note));
            System.out.println("PLAY SOUND " + inputStream.getFrameLength());
            clip.open(inputStream);
            clip.start();
            
            while (!clip.isRunning()) {
                Thread.sleep(10);
            }
            
            while (clip.isRunning()) {
                Thread.sleep(10);
            }
            
            clip.close();
            System.out.println("Note played");
        } 
        catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage());
        }
 
    }
}
 
abstract class Instrument {		// Defineste o metoda fara implementare
    abstract void play();
}
 
class Pian extends Instrument {		// Clasa care implementeaza interfata
 
    public void play() {			// Trebuie obligatoriu sa implementeze metoda play()
        System.out.println("Pian.play()");
        SoundControler.playSound("piano1.wav");
 
    }
}
 
class Vioara extends Instrument {	// Clasa care implementeaza interfata
 
    public void play() {			// Trebuie obligatoriu sa implementeze metoda play()
        System.out.println("Vioara.play()");
        SoundControler.playSound("string1.wav");
    }
}
 

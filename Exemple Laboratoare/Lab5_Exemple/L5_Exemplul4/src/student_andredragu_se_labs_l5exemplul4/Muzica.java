package student_andredragu_se_labs_l5exemplul4;

public class Muzica { 		// Clasa principala
	 
    static void play(Instrument i) {		// Metoda statica care porneste un instrument generic ce implementeaza interfata Instrument
        i.play();
    }
 
    static void playAll(Instrument[] e) {
        for (int i = 0; i < e.length; i++) {
            play(e[i]);
        }
    }

    public static void main(String[] args) {
        Instrument[] orchestra = new Instrument[2];
        int i = 0;
        orchestra[i++] = new Pian();
        orchestra[i++] = new Vioara();
        playAll(orchestra);
 
    }
}
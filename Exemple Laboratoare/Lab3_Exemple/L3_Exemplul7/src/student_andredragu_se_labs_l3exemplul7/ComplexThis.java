// Exemplul 7, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa ComplexThis
// Implementarea unei clase care pune in evidenta cuvantul cheie this

package student_andredragu_se_labs_l3exemplul7;

public class ComplexThis {			// Clasa ComplexThis
	int real;						// Partea reala a numarului complex
	int imaginar;					// Partea imaginara a numarului complex
	
	ComplexThis add(int i, int j) {		// Metoda add a clasei ComplexThis, care va returna obiectul curent
		real = real + i;				
        imaginar = imaginar + j;
        return this;					// Returnarea obiectului curent
	}
}
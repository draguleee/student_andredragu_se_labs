// Exemplul 3, Laboratorul 3, sectiunea Principiile POO
// Clasa SenzorReferinta
// Implementarea unei clase in care se evidentiaza referintele

package student_andredragu_se_labs_l3exemplul3;

public class SenzorReferinta{		// Clasa SenzorReferinta
    int value;						// Valoarea pe care o citeste senzorul
    boolean active;					// Valoarea de adevar a senzorului
 
    void setActive() {												// Setter pentru valoarea de adevar a senzorului
    	active = !active;									 		// Negarea valorii de adevar a variabilei booleene active
    	System.out.println("Sensor active status = " + active);		// Afisarea starii senzorului
    }
 
    void setValue(int k) {													// Setter pentru valoarea senzorului
    	if(k < 0 || k > 100)												// Daca k < 0 sau k > 100...
    		System.out.println("Invalid value. Senzor value unchanged!");	// ... se va afisa mesajul de "Valoare Invalida"
    	else {																// Daca k > 0 sau k < 100...							
    		value = k;														// ... lui value i se asociaza valoarea lui k
    		System.out.println("Sensor value = " + value); 					// ... se va afisa valoarea senzorului					
    	}
    }
 
    int getValue() {		// Getter pentru valoarea senzorului
    	return value;		// Returnarea valorii senzorului
    }
    
    public static void main(String[] args) {						// Metoda main()
    	
		SenzorReferinta s1 = new SenzorReferinta();					// Creare senzor 1
		SenzorReferinta s2 = new SenzorReferinta();					// Creare senzor 2
		SenzorReferinta s3 = null;									// Creare senzor 3
 
		s1.setActive();												// Setarea valorii de adevar a senzorului
		s1.setValue(190);											// Setarea valorii senzorului (valoare prea mare -> Valoare Invalida)
		s1.setValue(67);											// Setarea valorii senzorului (valoare buna -> Valoare Senzor)
 
		System.out.println(" ");
		
		System.out.println("Senzor s1 value = " + s1.getValue());	// Afisarea valorii senzorului 1 ( = 67)
		System.out.println("Senzor s2 value = " + s2.getValue());	// Afisarea valorii senzorului 2 ( = 0)
		
		s2 = s1;													// Senzorului 2 i se asociaza valoarea senzorului 1
		s3 = s1;													// Senzorului 3 i se asociaza valoarea senzorului 1
		
		System.out.println(" ");						
		
		System.out.println("Senzor s2 value = " + s2.getValue());	// Afisarea valorii senzorului 2 ( = 67)
		System.out.println("Senzor s3 value = " + s3.getValue());	// Afisarea valorii senzorului 3 ( = 67)
    }
 }    
 

 
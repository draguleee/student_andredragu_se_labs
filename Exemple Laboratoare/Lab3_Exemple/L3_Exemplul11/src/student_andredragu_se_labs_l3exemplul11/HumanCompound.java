// Exemplul 11, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa HumanCompound
// Implementarea unei clase care evidentiaza obiectele compuse

package student_andredragu_se_labs_l3exemplul11;

public class HumanCompound {
	Head head;			// head, care este de tipul Head
	Leg legL, legR;		// legL si legR, care sunt de tipul Leg
	Arm armL, armR;		// armL si armR, care sunt de tipul Arm
	Body body;			// body, care este de tipul Body
 
	HumanCompound() {				// Constructor pentru Human
		head = new Head();	// Creare cap
		legL = new Leg();	// Creare picior stang
		legR = new Leg();	// Creare picior drept
		armL = new Arm();	// Creare mana stanga
		armR = new Arm();	// Creare mana dreapta
		body = new Body();	// Creare corp
	}
 
	public static void main(String[] args) {
		HumanCompound h1 = new HumanCompound();		// Creare Human
	}
}
 
class Leg{}		// Clasa Leg (clasa goala)
 
class Head{}	// Clasa Head (clasa goala)
 
class Body{}	// Clasa Body (clsa goala)
 
class Arm{}		// Clasa Arm (clasa goala)
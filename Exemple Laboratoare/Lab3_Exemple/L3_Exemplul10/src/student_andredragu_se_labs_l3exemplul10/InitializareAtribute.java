// Exemplul 10, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa InitializareAtribute
// Implementarea unei clase care initializeaza atribute

package student_andredragu_se_labs_l3exemplul10;

public class InitializareAtribute {		// Clasa InitializareAtribute
	boolean t;			// t - tip boolean
    char c;				// c - tip caracter
    byte b;				// b - tip byte
    short s;			// s - tip short
    int i;				// i - tip intreg
    long l;				// l - tip long
    float f;			// f - real
    double d;			// d - double

    void print(String s) {			// Metoda print()
    	System.out.println(s);
    }

    void test() {					// Metoda test()
    	print("Data type      Initial value");		
    	print("boolean        " + t);
    	print("char           [" + c + "]");
    	print("byte           " + b);
    	print("short          " + s);
    	print("int            " + i);
    	print("long           " + l);
    	print("float          " + f);
    	print("double         " + d);
    }

   public static void main(String[] args) {		// Metoda main()
	   InitializareAtribute ia = new InitializareAtribute();
	   ia.test();
   }
}
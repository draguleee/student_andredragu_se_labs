// Exemplul 4, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa FlowerConstructorImplicit
// Implementarea unei clase care are constructor implicit

package student_andredragu_se_labs_l3exemplul4;

public class FlowerConstructorImplicit {					// Clasa FlowerConstructorImplicit
	int petal;												// Numarul de petale al florii
    
	FlowerConstructorImplicit() {							// Constructorul implicit 
    	System.out.println("Flower has been created!");     // Afisarea mesajului "Flower has been created!"
    }

	public static void main(String[] args) {	// Metoda main()
		FlowerConstructorImplicit[] garden = new FlowerConstructorImplicit[5];		// Crearea unui array care contine 5 flori
		for(int i = 0; i < 5; i++) {												// Pentru in in intervalul [0,5)...
			FlowerConstructorImplicit f = new FlowerConstructorImplicit();			// ... se creeaza o noua floare
			garden[i] = f;															// ... se introduce floarea creata in gradina (array)
		}
	}
}
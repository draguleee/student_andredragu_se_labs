// Exemplul 6, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa RobotSupraincarcare
// Implementarea unei clase care are metode si constructori supraincarcati

package student_andredragu_se_labs_l3exemplul6;

public class RobotSupraincarcare {		// Clasa RobotSupraincarcare
	int arms;							// Maiinile robotului
    int legs;							// Picioarele robotului
    int position;						// Pozitia robotului

    RobotSupraincarcare(int a) {			// Constructor 1
                arms = a;				
                legs = a;
                position =0;
    }
    
    RobotSupraincarcare(int a, int l) {		// Constructor 2
                arms = a;
                legs = l;
                position=0;
    }
    
    void move() {																	// Metoda move()
    	position++;        															// Incrementarea pozitiei
    	System.err.println("Robot is moving. [position = " + position + " ]");		// Afisarea pozitiei robotului
    }
    
    void move(int increment) {													// Metoda move, de data aceasta utilizandu-se intregul increment
    	position = position + increment;										// Incrementarea pozitiei
    	System.err.println("Robot is moving. [position = " + position + " ]");	// Afisarea pozitiei robotului
    }        
    
    public static void main(String[] args) {		// Metoda main()
    	
    	RobotSupraincarcare r1 = new RobotSupraincarcare(2,2);		// Creare robot cu al doileaconstructor
    	RobotSupraincarcare r2 = new RobotSupraincarcare(4);		// Creare robot cu primul constructor
    	
    	/* Instructiune invalida
    	RobotSupraincarcare r3 = new RobotSupraincarcare();
    	 */
                
    	r1.move();
        r1.move(10);
        r2.move();
        r2.move(5);
    }
}
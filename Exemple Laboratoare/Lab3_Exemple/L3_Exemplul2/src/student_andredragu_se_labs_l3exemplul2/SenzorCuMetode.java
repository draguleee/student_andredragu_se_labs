// Exemplul 2, Laboratorul 3, sectiunea Principiile POO
// Clasa SenzorCuMetode
// Implementarea unei clase cu metode

package student_andredragu_se_labs_l3exemplul2;

public class SenzorCuMetode{		// Clasa SenzorCuMetode
    int value;						// Valoarea pe care o citeste senzorul
    boolean active;					// Valoarea de adevar a senzorului
 
    void setActive() {												// Setter pentru valoarea de adevar a senzorului
    	active = !active;											// Negarea valorii de adevar a variabilei booleene active			
    	System.out.println("Sensor active status = " + active);		// Afisarea starii senzorului
    }
 
    void setValue(int k) {													// Setter pentru valoarea senzorului
    	if(k < 0 || k > 100)												// Daca k < 0 sau k > 100...
    		System.out.println("Invalid value. Senzor value unchanged!");   // ... se va afisa mesajul de "Valoare Invalida" 
    	else {																// Daca k > 0 sau k < 100...
    		value = k;														// ... lui value i se asociaza valoarea lui k 
    		System.out.println("Sensor value = " + value);					// ... se va afisa valoarea senzorului
    	}
    }
 
    int getValue() {		// Getter pentru valoarea senzorului
    	return value;		// Returnarea valorii senzorului
    }
 
    public static void main(String[] args) {					// Metoda main()
    	
		SenzorCuMetode s1 = new SenzorCuMetode();				// Creare senzor 1
		SenzorCuMetode s2 = new SenzorCuMetode();				// Creare senzor 2
 
		s1.setActive();											// Setarea valorii de adevar a senzorului
		s1.setValue(190);										// Setarea valorii senzorului (valoare prea mare -> Valoare Invalida)
		s1.setValue(67);										// Setarea valorii senzorului (valoare buna -> Valoarea Senzorului)
		int output1 = s1.getValue();
		System.out.println("Sensor's value: " + output1);		// Afisarea valorii senzorului
		
		System.out.println(" ");
		
		s2.setActive();											// Setarea valorii de adevar a senzorului
		s2.setValue(-10);										// Setarea valorii senzorului (valoare prea mica -> Valoare Invalida)
		s2.setValue(39);										// Setarea valorii senzorului (valoare buna -> Valoarea Senzorului)
		int output2 = s2.getValue();
		System.out.println("Sensor's value: " + output2); 		// Afisarea valorii senzorului
 
	}
}
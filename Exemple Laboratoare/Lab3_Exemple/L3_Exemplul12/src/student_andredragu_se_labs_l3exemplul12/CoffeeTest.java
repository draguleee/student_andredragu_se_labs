// Exemplul 12, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa CoffeeTest
// Realizarea unei aplicatii formata din mai multe clase 

package student_andredragu_se_labs_l3exemplul12;

public class CoffeeTest {		// Clasa CoffeeTest (clasa de baza)
	
	public static void main(String[] args) {			// Functia main
		
		CoffeeMaker maker1 = new CoffeeMaker();   		// Creare coffe maker
		Coffee[] pachet = new Coffee[10];				// Creare pachet de cafea
		
		for (int i = 0; i < pachet.length; i++) {		// Pentru i de la 0 pana la lungimea pachetului
			pachet[i] = maker1.getCoffee();				// ... se va returna cafeaua
		}
		
		for (int i = 0; i < pachet.length; i++) {		// Pentru i de la 0 pana la lungimea pachetului
			pachet[i].drinkCoffee();					// ... se va bea cafeaua
		}
	}
}
	 
class CoffeeMaker {								// Clasa CoffeeMaker
	CaffeineTank ctank = new CaffeineTank();	// Creare rezervor de cafea
	WaterTank wtank = new WaterTank();			// Creare rezervor de apa
	
	CoffeeMaker() {											// Constructor pentru coffee maker
		System.out.println("New cofee maker created.");		// Afisarea mesajului "New coffee maker created."
	}
	
	Coffee getCoffee() {					// Metoda getCoffee() de tip Coffee
		int w = wtank.getIngredient();		
		int c = ctank.getIngredient();
		return new Coffee(w,c);				// Returnare cafea
	}
}

class  CaffeineTank {										// Clasa CaffeineTank
	
	CaffeineTank() {										// Constructor pentru rezervorul de cafea 
		System.out.println("New coffeine tank created.");	// Afisarea mesajului "New caffeine tank created"
	}
	            
	int getIngredient() {					// Metoda getIngredient()
		return (int)(Math.random()*10);		// Returnarea unui numar intreg generat in mod aleatoriu
	}
}

class WaterTank {											// Clasa WaterTank
		
	WaterTank() {											// Constructor pentru rezervorul de apa
		System.out.println("New water tank created.");		// Afisarea mesajului "New water tank created"
	}
	            
	int getIngredient() {					// Metoda getIngredient()
		return (int)(Math.random()*40);		// Returnarea unui numar intreg generat in mod aleatoriu
	}
}

class Coffee {			// Clasa Coffee
	int water;			// Cantitatea de apa folosita pentru cafea
	int caffeine;		// Cantitatea de cafeina folosita pentru cafea
	            
	Coffee(int w, int c) {		// Constructor pentru cafea
		this.water = w;			
		this.caffeine= c;
	}
	
	void drinkCoffee() {															// Metoda drinkCoffee()
		System.out.println("Drink cofee [water="+water+":coffe="+ caffeine+"]");
	}
}
// Exemplul 13, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa SmartphoneV2
// Implementarea unei aplicatii formata din mai multe clase

package student_andredragu_se_labs_l3exemplul13;

class Baterie {		// Clasa Baterie
	int pb;			// pb - procentaj baterie (de tip intreg)
	
	Baterie() {		// Constructor baterie
		pb = 100;	// Initializare procentaj baterie cu 100
	}
 
	boolean utilizeaza(int x) {							// Metoda utilizeaza(x) de tip boolean
		if(pb <= x) {									// Daca pb <= x
			System.out.println("Baterie descarcata!");	// Se va afisa baterie descarcata
			return false;								// Returnare false
		}
		pb = pb - x;									
		return true;									// Returnare true
	}
 
	void incarca() {		// Metoda incarca()
		if(pb < 100)		// Daca pb < 100
			pb++;			// Se incrementeaza procentajul bateriei
		afiseaza();			// Se afiseaza procentajul bateriei
	}
 
	void afiseaza() {								// Metoda afiseaza()
		System.out.println("Baterie " + pb + "%");	// Afisarea procentajului bateriei		
	}
}
 
public class SmartphoneV2 {			// Clasa SmartphoneV2
	static int contor;				// Variabila statica contor de tip intreg
	String model;					// Variabila model de tip String
	int k;							// Variabila k de tip int
	Baterie b = null;				// Variabila b de tip Baterie
 
	SmartphoneV2(String model, int k) {							// Constructor 1
		contor++;												// Incrementare contor
		this.model = model;					
		this.k = k;
		System.out.println("Telefon " +model+" construit.");	// Afisare telefon construit
		afiseazaBaterie();										// Afisarea procentajului bateriei
	}
 
	void afiseazaBaterie() {									// Metoda afiseazaBaterie()
		if(b!=null)												// Daca bateria este diferita de valoarea null
			b.afiseaza();										// Se afiseaza bateria
		else													// Altfel
			System.out.println("Telefonul nu are baterie!");	// Se afiseaza mesajul "Telefonul nu are baterie!"
	}
 
	void incarca() {											// Metoda incarca()
		if(b!=null)												// Daca bateria este diferita de valoarea null
			b.incarca();										// Se incarca bateria
		else													// Altfel
			System.out.println("Telefonul nu are baterie!");	// Se afiseaza mesajul "Telefonul nu are baterie!"
	}
 
	void seteazaBaterie(Baterie b) {							// Setter pentru baterie
		if(b!=null) {											// Daca bateria este diferita de valoarea null
			System.out.println("Se inlocuieste baterie!");		// Se afiseaza mesajul "Se inlocuieste baterie!"
		}
		else {													// Altfel daca bateria este egala cu valoarea null
			System.err.println("Se instaleaza baterie!");		// Se afiseaza mesajul "Se instaleaza baterie!"
		}
		this.b = b;
	}
 
	void apeleaza() {											// Metoda apeleaza()
		System.out.println("Apelare");							// Afisare "Apelare"
		if(b==null) {											// Daca bateria este egala cu valoarea null		
			System.out.println("Telefonul nu are baterie.");	// Se afiseaza mesajul "Telefonul nu are baterie!"
			return;								
		}
		if(b.utilizeaza(k)==false) {							// Daca telefonul nu este utilizat
			System.out.println("Baterie telefon descarcata.");	// Se afiseaza mesajul "Bateria este descarcata!"
		}
		else {															// Altfel daca telefonul este utilizat
			System.out.println("Telefonul "+model+" este utilizat.");	// Se afiseaza mesajul "Telefonul este utilizat"
		}
	}
 
	public static void main(String[] args) {					// Metoda main()
 
		SmartphoneV2 t4 = new SmartphoneV2("Nexus 4",3);
 
		t4.apeleaza();
 
		Baterie b1 = new Baterie();
 
		t4.seteazaBaterie(b1);
 
		t4.apeleaza();
 
		System.out.println("Telefone construite = "+SmartphoneV2.contor);
	}
}
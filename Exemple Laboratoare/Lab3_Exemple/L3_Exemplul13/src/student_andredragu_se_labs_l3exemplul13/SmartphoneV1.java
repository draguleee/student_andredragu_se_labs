// Exemplul 13, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa SmartphoneV1
// Implementarea unei aplicatii compusa din mai multe clase

package student_andredragu_se_labs_l3exemplul13;

public class SmartphoneV1 {
	static int contor;		// Variabila statica contor de tip int
	String model;			// Variabila model de tip String
	int pb;					// Variabila pb (procentaj baterie) de tip int
	int k;					// Variabila k de tip int
 
	SmartphoneV1() {											// Constructor 1
		contor++;												// Incrementarea contorului
		k = 1;													// Initializarea variabilei k cu 1
		model = "Samsung S4";									// Initializarea variabilei model cu string-ul "Samsung S4"
		System.out.println("Telefon " + model + " construit.");	// Afisarea modelului de telefon construit 
		pb = 100;												// Initializarea variabilei pb cu 100 
		afiseazaBaterie();										// Afisarea bateriei telefonului
	}
 
	SmartphoneV1(String model) {								// Constructor 2
		contor++;												// Incrementarea contorului
		this.model = model;										
		k = 1;													// Initializarea variabilei k cu 1
		System.out.println("Telefon " +model+" construit.");	// Afisarea modelului de telefon construit
		pb = 100;												// Initializarea variabilei pb cu 100
		afiseazaBaterie();										// Afisarea bateriei telefonului
	}
 
	SmartphoneV1(String model, int k) {							// Constructor 3
		contor++;												// Incrementarea contorului
		this.model = model;		
		this.k = k;
		System.out.println("Telefon " +model+" construit.");	// Afisarea modelului de telefon construit
		pb = 100;												// Initializarea variabilei pb cu 100
		afiseazaBaterie();										// Afisarea bateriei telefonului
	}
 
	void afiseazaBaterie() {									// Metoda afiseazaBaterie()
		System.out.println("Baterie=" + pb + "% model="+model);	// Afisarea procentajului bateriei pentru un model de telefon
	}
 
	void incarca() {									// Metoda incarca()
		if (pb < 100)									// Daca procentajul bateriei este mai mic decat 100...
			pb++;										// ... se incrementeaza pb
		afiseazaBaterie();								// ... se afiseaza bateria telefonului
		System.out.println("Incarcat="+pb+"%");
	}
 
	void apeleaza() {														// Metoda apeleaza()
		if (pb <= k) {														// Daca pb <= k...
			System.out.println("Telefon "+ model + " descarcat!");			// ... telefonul este descarcat
		} else {															// Altfel, daca pb > k...
			System.out.println("Telefonul " + model + " este utilizat.");	// ... telefonul este utilizat
			pb = pb - k;														
			afiseazaBaterie();												// Afisarea bateriei
		}
	}
 
	public static void main(String[] args) {
		
		SmartphoneV1 t1 = new SmartphoneV1();				// Creare smartphone 1 cu ajutorul primului constructor
		SmartphoneV1 t2 = new SmartphoneV1();				// Creare smartphone 2 cu ajutorul primului constructor 
		SmartphoneV1 t3 = new SmartphoneV1("Nexus 4");		// Creare smartphone 3 cu ajutorul celui de-al doilea constructor
		SmartphoneV1 t4 = new SmartphoneV1("Nexus 4",3);	// Creare smartphone 4 cu ajutorul celui de-al treilea constructor
 
		t1.apeleaza();
		t2.apeleaza();
		t3.apeleaza();
		t4.apeleaza();
 
		t1.apeleaza();
		t1.apeleaza();
		for(int i = 0; i < 5; i++)
			t1.incarca();
 
		t2.apeleaza();
		t1.apeleaza();
 
		for(int i = 0; i < 5; i++)
			t1.apeleaza();
 
		System.out.println("Telefone construite = " + SmartphoneV1.contor);
	}
}
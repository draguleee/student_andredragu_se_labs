// Exemplul 8, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa EngineThis
// Implementarea unei clase prin evidentierea cuvantului cheie this

package student_andredragu_se_labs_l3exemplul8;

public class EngineThis {		// Clasa EngineThis
    String fuellType;			// Tipul de combustibil (String)
    long capacity;				// Capacitatea motorului (long)
    boolean active;				// Starea motorului (boolean)

    EngineThis(int c, boolean a) {		// Constructor 1
    	this.capacity = c;
    	this.active = a;
    }          
    
    EngineThis(int c, boolean a, String fT) { 	// Constructor 2
    	this(c,a);
    	/* Mai putem scrie astfel:
    	 * this.capacity = c;
    	 * this.active = a
    	 */
    	this.fuellType = fT;
    }        
    
    EngineThis() {						// Constructor implicit
    	this(2000,false,"diesel");
    	/* Mai putem scrie astfel:
    	 * this.capacity = 2000;
    	 * this.active = false;
    	 * this.fuellType = "diesel"; 
    	 */
    }    
    
    void print() {		// Metoda print(), care afiseaza informatiile despre motor
    	System.out.println("Engine: capacity = " + this.capacity + "," + " fuell = " + fuellType + "," + " active = " + active +".");
    }
    public static void main(String[] args) {
    	
    	EngineThis tdi = new EngineThis();						// Creare motor 1 cu constructorul implicit
        EngineThis i16 = new EngineThis(1600,false,"petrol");	// Creare motor 2 cu constructorul 2
        EngineThis d30 = new EngineThis(3000,true,"diesel");	// Creare motor 3 cu constructorul 2
        
        tdi.print();		// Afisare informatii motor 1
        i16.print();		// Afisare informatii motor 2	
        d30.print();		// Afisare informatii motor 3
    }
}
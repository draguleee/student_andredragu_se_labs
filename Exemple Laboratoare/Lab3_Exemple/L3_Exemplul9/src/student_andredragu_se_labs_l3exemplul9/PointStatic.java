// Exemplul 9, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa PointStatic
// Implementarea unei clase care evidentiaza cuvantul cheie static

package student_andredragu_se_labs_l3exemplul9;

public class PointStatic {		// Clasa PointStatic
	int x;						// Coordonata x a punctului
	static int y;				// Coordonata y a punctului, declarata static

    void displayPoint() {					// Metoda displayPoint(), care afiseaza punctul
    	System.out.println("x = " + x);		// Afisarea coordonatei x
    	System.out.println("y = " + y);		// Afisarea coordonatei y
    	System.out.println("---");
    }

    static void setY(int val) {		// Setter pentru coordonata y a punctului
    	y = val;					// Variabilei statice y i se asociaza valoarea lui val
    }

    public static void main(String[] args) {	// Metoda main()
    	
    	PointStatic p1 = new PointStatic();		// Creare punct 1 cu constructorul implicit
    	PointStatic p2 = new PointStatic();		// Creare punct 2 cu constructorul implicit
    	
    	p1.x = 10;
    	p1.y =15;
    	p1.displayPoint();			// Afisare punct cu coordonatele [10,15]

    	p2.x = 256;				
    	p2.y = 128;
    	p2.displayPoint();			// Afisare punct cu coordonatele [256,128]
    	
    	p1.displayPoint();			// Afisare punct cu coordonatele [10,128] (y - static, isi pastreaza ultima valoare asociata)
    	
    	PointStatic.setY(333);		// Setarea coordonate y ca fiind 333
    	
    	p1.displayPoint();			// Afisare punct cu coordonatele [10,333]
        p2.displayPoint();			// Afisare punct cu coordonatele [256,333]

    }
}
// Exemplul 5, Laboratorul 3, sectiunea Ciclul de Viata al Obiectelor
// Clasa FlowerConstructor
// Implementarea unei clase care are un constructor definit de utilizator

package student_andredragu_se_labs_l3exemplul5;

public class FlowerConstructor {							// Clasa FlowerConstructor
    int petal;												// Numarul de petale al florii
    
    /* Constructorul Implicit
    FlowerConstructor() {}
    */		
    
    FlowerConstructor(int p) {       							// Constructor
    	petal = p;												// Variabila petal va lua valoarea lui p
    	System.out.println("New flower has been created!");		// Afisarea mesajului "New Flower has been created!"
    }

    public static void main(String[] args) {
    	
    	FlowerConstructor f1 = new FlowerConstructor(4);
    	FlowerConstructor f2 = new FlowerConstructor(6);
    	
    	/* Eroare de compilare - stergeti linia pentru a putea rula
        FlowerConstructor f3 = new FlowerConstructor();
        */
    }
}
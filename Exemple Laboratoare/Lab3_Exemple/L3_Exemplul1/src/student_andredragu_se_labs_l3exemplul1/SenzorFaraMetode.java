// Exemplul 1, Laboratorul 3, sectiunea Principiile POO
// Clasa SenzorFaraMetode
// Implementarea unei clase fara metode

package student_andredragu_se_labs_l3exemplul1;

public class SenzorFaraMetode {								// Clasa SenzorFaraMetode
    int value;												// Valoarea pe care o citeste senzorul
    boolean active;											// Valoarea de adevar a senzorului 
 
    public static void main(String[] args) {				// Metoda main()
    	
		SenzorFaraMetode s1 = new SenzorFaraMetode();		// Creare senzor 1
		SenzorFaraMetode s2 = new SenzorFaraMetode();		// Creare senzor 2
		SenzorFaraMetode s3 = null; 						// Creare senzor 3 - nu;;
 
		System.out.println("Senzor s1 value = " + s1.value);	// Afisarea valorii senzorului 1
		System.out.println("Senzor s2 value = " + s2.value);	// Afisarea valorii senzorului 2
		System.out.println("Senzor s3 value = " + s3.value);	// Afisarea valorii senzorului 3
	}
}
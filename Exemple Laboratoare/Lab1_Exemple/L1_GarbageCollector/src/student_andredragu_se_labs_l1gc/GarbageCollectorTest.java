// Exemplul Garbage Collector, laboratorul 1

package student_andredragu_se_labs_l1gc;

public class GarbageCollectorTest {			// Clasa GarbageCollectorTest
    static int removedObjects;				// Variabila statica removedObjects, mostenita din cadrul clasei GarbageCollector
    
    public static class Flower {			// Clasa statica Flower
    	String name;						// Numele florii
    		
    	Flower(String name) {				// Constructor pentru floare
    		this.name = name;
    	}                      
                
    	public void finalize() {			// Metoda finalize(), care este apelata de Garbage Collector in momentul in care obiectul urmeaza sa fie sters
    		removedObjects++;
    		System.err.println("The flower "+name+" is removed. Number of removed flowers is "+removedObjects);
    	}
    }
    
    public static void main(String[] args) {	// Metoda main()
    	Flower myF = null;												
    	for(int i = 0; i < 10000; i++)
    		myF = new Flower(" Flower "+i);
    }
}

// Exemplul GarbageCollectorAnotherTest

package student_andredragu_se_labs_l1gc;

public class GarbageCollectorAnotherTest {			// Clasa GarbageCollectorAnotherTest
    static int removedObjects;						// Variabila statica removedObjects, mostenita din cadrul clasei GarbageCollector
    
    public static class Flower {					// Clasa statica Flower
    	String name;								// Numele florii
    	
    	Flower(String name) {						// Constructor pentru floare
    		this.name = name;
    	}                       
    	
    	public void finalize() {        			// Metoda finalize(), care este apelata de Garbage Collector in momentul in care obiectul urmeaza sa fie sters
    		removedObjects++;
    		System.err.println("The flower "+name+" is removed. Number of removed flowers is "+removedObjects);
    	}
    }

    public static void main(String[] args) {		// Metoda main()
    	Flower myF = null;
    	int i = 0;
    	while(true) {								// Atat timp cat conditia este adevarata...
    		i++;									// ... se incrementeaza i
    		myF = new Flower("Flower "+i);			// ... se creeaza florile
    	}
    }
}
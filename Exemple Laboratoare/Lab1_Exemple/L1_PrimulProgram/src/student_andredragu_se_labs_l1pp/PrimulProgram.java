// Exemplul primul program, laboratorul 1

package student_andredragu_se_labs_l1pp;

import java.util.*;

public class PrimulProgram {								// Clasa PrimulProgram
	
    public static void main(String[] args) {				// Metoda main()
    	
    	System.out.println("Hello, the current date is: ");		// Afisarea textului "Hello, the current date is:"
    	Date data = new Date();									// Crearea unei noi date
    	System.out.println(data);								// Afisarea datei curente
}
}
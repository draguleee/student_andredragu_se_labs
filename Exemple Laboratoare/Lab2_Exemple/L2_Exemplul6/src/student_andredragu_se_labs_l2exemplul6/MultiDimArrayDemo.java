// Exemplul 6, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu tablou multidimensional

package student_andredragu_se_labs_l2exemplul6;

public class MultiDimArrayDemo {							// Clasa MultiDimArrayDemo
	
    public static void main(String[] args) {				// Metoda main()
        
    	String[][] names = {								// Crearea unui tablou multidimensional
            {"Mr. ", "Mrs. ", "Ms. "},
            {"Smith", "Jones"}
        };
    	
        // Afisare Mr. Smith
        System.out.println(names[0][0] + names[1][0]);
        
        // Afisare Ms. Jones
        System.out.println(names[0][2] + names[1][1]);
    }
}
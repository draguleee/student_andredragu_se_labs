// Exemplul 1, Laboratorul 1, sectiunea Structuri Lexicale
// Exemplu if-else, clasa IfElseDemo
// Acest exemplu afiseaza un calificativ de la A la F in functie de scorul obtinut intr-un test
// Scorul obtinut la test se citeste de la tastatura

package student_andredragu_se_labs_l2exemplul1;

import java.util.Scanner;

public class IfElseDemo_v2 {
	
	// Functia main
	public static void main(String[] args) {
		
		Scanner s = new Scanner(System.in);
		int testscore;											// Declarare testscore ca intreg
		System.out.print("Scorul obtinut in test: ");			// Cere utilizatorului sa introduca scorul obtinut la test
		testscore = s.nextInt();
		
		char grade;
 
        if (testscore >= 90) {						// Daca scorul de la test este mai mare sau egala cu 90...
            grade = 'A';							// ... atunci vom avea calificativul A
        } else if (testscore >= 80) {				// Daca scorul de la test este mai mare sau egala cu 80...
            grade = 'B';							// ... atunci vom avea calificativul B
        } else if (testscore >= 70) {				// Daca scorul de la test este mai mare sau egal cu 70...
            grade = 'C';							// ... atunci vom avea calificativul C
        } else if (testscore >= 60) {				// Daca scorul de la test este mai mare sau egal cu 60...
            grade = 'D';							// ... atunci vom avea calificativul D
        } else {									// Daca avem orice alt scor mai mic decat 60...
            grade = 'F';							// ... atunci vom avea calificativul F
        }
        System.out.println("Grade = " + grade);		// Afisarea calificativului
		
	}

}

// Exemplul 2, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu switch, clasa SwitchDemo
// Acest exemplu afiseaza o luna de la January la December in functie de numarul lunii
// Numarul lunii este initializate in functia main

package student_andredragu_se_labs_l2exemplul2;

public class SwitchDemo {
	
	// Functia main
    public static void main(String[] args) {
 
        int month = 8;									// Declarare month ca int si initializarea acestiua cu 8
        String monthString;								// Declarare monthString ca string, va reprezenta numele lunii corespunzatoare numarului 
        switch (month) {
            case 1:  monthString = "January";			// Cazul 1: daca month = 1, vom avea luna January
                     break;
            case 2:  monthString = "February";			// Cazul 2: daca month = 2, vom avea luna February
                     break;
            case 3:  monthString = "March";				// Cazul 3: daca month = 3, vom avea luna March
                     break;
            case 4:  monthString = "April";				// Cazul 4: daca month = 4, vom avea luna April
                     break;
            case 5:  monthString = "May";				// Cazul 5: daca month = 5, vom avea luna May
                     break;
            case 6:  monthString = "June";				// Cazul 6: daca month = 6, vom avea luna June
                     break;
            case 7:  monthString = "July";				// Cazul 7: daca month = 7, vom avea luna July
                     break;
            case 8:  monthString = "August";			// Cazul 8: daca month = 8, vom avea luna August
                     break;
            case 9:  monthString = "September";			// Cazul 9: daca month = 9, vom avea luna September
                     break;
            case 10: monthString = "October";			// Cazul 10: daca month = 10, vom avea luna October
                     break;	
            case 11: monthString = "November";			// Cazul 11, daca month = 11, vom avea luna November
                     break;
            case 12: monthString = "December";			// Cazul 12, daca month = 12, vom avea luna December
                     break;
            default: monthString = "Invalid month";		// Pentru orice alt numar, se va afisa mesajul "Invalid month"
                     break;
        }
        System.out.println("Month corresponding to its number: " +monthString);				// Afisarea numelui lunii in functie de numarul asociat variabilei month
    }
}
// Exemplul 5, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu for, clasa ForDemo
// Acest exemplu efectueaza numaratoarea pentru un count intre countMin si countMax, introdusi de la tastatura

package student_andredragu_se_labs_l2exemplul5;

import java.util.Scanner;

public class ForDemo_v2 {
	
	// Functia main
    public static void main(String[] args){
    	
    	Scanner s = new Scanner(System.in);
    	
    	int count;
    	int countMin;
    	int countMax;
    	
    	System.out.println("Enter the number where the count starts: ");
    	countMin = s.nextInt();
    	System.out.println("Enter the number where the count ends: ");
    	countMax = s.nextInt();
         for(count = countMin; count < countMax; count++) {
              System.out.println("Count is: " + count);
         }
    }
}
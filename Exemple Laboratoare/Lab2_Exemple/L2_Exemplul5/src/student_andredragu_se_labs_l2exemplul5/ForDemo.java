// Exemplul 5, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu for, clasa ForDemo
// Acest exemplu efectueaza numaratoarea pentru un count intr-un interva ales

package student_andredragu_se_labs_l2exemplul5;

class ForDemo {
	
	// Functia main
    public static void main(String[] args) {
    	
    	int count;
         for(count = 1; count < 11; count++) {						// Pentru un i intreg de la i la 11...
              System.out.println("Count is: " + count);				// Se va efectua numaratoarea
         }
    }
}
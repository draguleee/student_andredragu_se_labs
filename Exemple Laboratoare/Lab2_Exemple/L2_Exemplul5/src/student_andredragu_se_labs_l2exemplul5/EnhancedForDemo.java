// Exemplul 5, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu for, clasa EnhancedForDemo
// Acest exemplu efectueaza numaratoarea pentru o colectie care va trebui parcursa de un iterator

package student_andredragu_se_labs_l2exemplul5;

public class EnhancedForDemo {								// Clasa EnhancedForDemo
	
    public static void main(String[] args) {				// Metoda main()
    	
        int[] numbers = {1,2,3,4,5,6,7,8,9,10};				// Colectia de numere care trebuie parcursa
        for (int item : numbers) {							// item este iteratorul care va parcurge colectia de numere
            System.out.println("Count is: " + item);		// Afisarea numaratorii
        }
   }
}
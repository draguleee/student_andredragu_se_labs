// Exemplul 7, Laboratorul 2, sectiunea Structuri Lexicale

package student_andredragu_se_labs_l2exemplul7;

import java.util.*;

public class Hello {									// Clasa Hello
	
    public static void main(String[] args) {			// Metoda main()

    	System.out.println("Hi there!");				// Afisarea mesajului "Hi there!" in consola
 
        Random r = new Random();						
        
        int[] a = new int[10];							// Initializarea unui vector caruia i se aloca spatiu pentru 10 intregi
        
        for(int i = 0; i < a.length; i++) {				// Pentru i de la 1 la lungimea vectorului...
            a[i] = r.nextInt(100);						// ...se genereaza un element aleatoriu care apartine vectorului
        }
 
        for(int i = 0; i < a.length; i++) {				// Pentur i de la 1 la lungimea vectorului...
            System.out.print("a["+i+"]="+a[i]+" ");		// ... se afiseaza elementele generate care apartin vectorului
        }
    }    
}
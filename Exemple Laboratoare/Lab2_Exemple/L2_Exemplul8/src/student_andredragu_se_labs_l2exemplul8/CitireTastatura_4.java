// Exemplul 8, Laboratorul 2, sectiunea Citire de la tastatura
// Exemplu CitireTastatura_4
// Citire cu ajutorul clasei Console
// Acest exemplu nu functioneaza daca se ruleaza programul direct din Eclipse 
// Aceasta se datoreaza drepturilor pe care le are procesul Eclipse in cadrul sistemului de operare
// Pentru rularea programului, folositi linia de comanda si lansati programul cu comanda: java Test

package student_andredragu_se_labs_l2exemplul8;

import java.io.Console;

public class CitireTastatura_4 {
 
    public static void main(String[] args) throws Exception {
    	
        Console console = System.console();
        
        if (console == null) {
            System.out.println("Unable to fetch console");
            return;
        }
        
        String line = console.readLine();
        console.printf("I saw this line: %s", line);
    }
}
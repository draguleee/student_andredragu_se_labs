// Exemplul 8, Laboratorul 2, sectiunea Citire de la Tastatura
// Exemplu CitireTastatura_1
// Citire cu ajutorul BufferedReader
// Acest exemplu citeste de la tastatura un sir de caractere, dupa care un numar intreg

package student_andredragu_se_labs_l2exemplul8;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
 
public class CitireTastatura_1 {															// Clasa CitireTastatura_1
	
    public static void main(String[] args) throws IOException {								// Functia main() 
    	
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));			// Initializare BufferedReader
        
        System.out.print("Enter String:");													// Cere utilizatorului sa introduca un sir de caractere
        String s = br.readLine();															
 
        System.out.print("Enter Integer:");													// Cere utilizatorului sa introduca un numar intreg							
        int i = Integer.parseInt(br.readLine());
 
    }
}
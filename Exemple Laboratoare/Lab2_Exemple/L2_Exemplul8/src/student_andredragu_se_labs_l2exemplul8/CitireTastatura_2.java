// Exemplul 8, Laboratorul 2, sectiunea Citire de la Tastatura
// Exemplu CitireTastatura_2
// Citire cu ajutorul clasei Scanner
// Acest exemplu citeste de la tastatura un x intreg, dupa care citeste un y intreg

package student_andredragu_se_labs_l2exemplul8;

import java.util.Scanner;
 
public class CitireTastatura_2 {					// Clasa CitireTastatura_2 
	
   public static void main(String[] args) {			// Metoda main()
	   
       Scanner in = new Scanner(System.in);			// Initializare Scanner
       
       int x = in.nextInt();						// Introducerea lui x de la tastatura			
       int y = in.nextInt();						// Introducerea lui y de la tastatura
 
       System.out.println("x="+ x + "," + " y="+y);			// Afisarea lui x si a lui y
 
   } 
}  
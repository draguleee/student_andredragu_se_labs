// Exemplul 8, Laboratorul 2, sectiunea Citire de la Tastatura
// Exemplu CitireTastatura_3
// Citire cu ajutorul clasei Scanner
// Acest exemplu citeste de la tastatura un sir de caractere, dupa care un numar intreg

package student_andredragu_se_labs_l2exemplul8;

import java.util.Scanner;

public class CitireTastatura_3 {					// Clasa CitireTastatura_3
	
	public static void main(String[] args) {		// Metoda main()
		
		Scanner in = new Scanner(System.in);		// Initializare Scanner
		
		System.out.print("Introduceti valoarea lui x: ");		// Cere utilizatorului sa introduca valoarea lui x
		int x = in.nextInt();									// Citirea lui x de la tastatura
		
		System.out.print("Introduceti valoarea lui y: ");		// Cere utilizatorului sa introduca valoarea lui y
		int y = in.nextInt();
		
		System.out.println("x="+ x + "," + " y="+y);						// Afisarea lui x si a lui y
		
	}
}

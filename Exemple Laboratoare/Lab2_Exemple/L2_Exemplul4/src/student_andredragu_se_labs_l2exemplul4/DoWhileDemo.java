// Exemplul 4, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu do-while, clasa DoWhileDemo
// Acest exmeplu va efectua  numaratoarea de la un count initializat in main pana la un numar ales

package student_andredragu_se_labs_l2exemplul4;

public class DoWhileDemo {
	
	// Functia main
    public static void main(String[] args) {
        int count = 1;										// Declarare count ca int si initializarea lui cu 1
        
        do {												// Se executa urmatoarele...:
        	System.out.println("Count is: " + count);		// Afisarea numatatorii efectuate 
            count++;										// Incrementare
        } 
        while (count < 11);									// ... atat timp cat count este mai mic decat 11 
    }
}
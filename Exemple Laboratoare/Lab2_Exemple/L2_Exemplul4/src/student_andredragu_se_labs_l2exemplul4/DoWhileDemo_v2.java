// Exemplul 4, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu do-while, clasa DoWhileDemo
// Acest exmeplu va efectua  numaratoarea de la un countMin pana la un countMax

package student_andredragu_se_labs_l2exemplul4;

import java.util.Scanner;

public class DoWhileDemo_v2 {
	
	// Functia main
    public static void main(String[] args) {
    	
    	Scanner s = new Scanner(System.in);
        int countMin;										// Declarare countMin ca si int
        int countMax;										// Declarare countMax ca si int
        int count = 1;										// Declarare count ca si int si initializare la 1
        
    	System.out.println("Enter a number where to start the count: ");		// Cere utilizatorului sa introduca numarul de la care va incepe numaratoarea
    	countMin = s.nextInt();														
    	System.out.println("Enter a number where to end the count: ");			// Cere utilizatorului sa introduca numarul la care se va incheia numaratoarea
    	countMax = s.nextInt();
      
    	do {												// Se executa urmatoarele...:
        	System.out.println("Count is: " + count);		// Afisarea numatatorii efectuate 
            count++;										// Incrementare
        } while (count < 11);								// ... atat timp cat count este mai mic decat 11 
    }
}
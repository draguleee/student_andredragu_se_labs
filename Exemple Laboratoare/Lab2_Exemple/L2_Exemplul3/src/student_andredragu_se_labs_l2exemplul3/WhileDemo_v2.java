// Exemplul 3, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu while, clasa WhileDemo
// Acest exemplu efectueaza o numarare, atat timp cat count este mai mic decat un numar
// Se vor declara countMin si counMax si se vor introduce de la tastatura

package student_andredragu_se_labs_l2exemplul3;

import java.util.Scanner;

public class WhileDemo_v2 {
	
	// Functia main
    public static void main(String[] args) {
    	
    	Scanner s = new Scanner(System.in);
    	
    	int countMin;										// Numarul de la care va incepe numaratoarea
    	int countMax;										// Numarul la care se va opri numaratoarea
    	int count = 1;
    	System.out.println("Enter a number where to start the count: ");		// Cere utilizatorului sa introduca numarul de la care va incepe numaratoarea
    	countMin = s.nextInt();														
    	System.out.println("Enter a number where to end the count: ");			// Cere utilizatorului sa introduca numarul la care se va incheia numaratoarea
    	countMax = s.nextInt();
        while (count < countMax) {												// Atat timp cat count este mai mic decat valoarea maxima introdusa...
            System.out.println("Count is: " + count);							// ... se va afisa numaratoarea si...
            count++;															// ... se va incrementa corespunzator
        }
    }
}
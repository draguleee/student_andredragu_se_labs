// Exemplul 3, Laboratorul 2, sectiunea Structuri Lexicale
// Exemplu while, clasa WhileDemo
// Acest exemplu efectueaza o numarare, atat timp cat count este mai mic decat un numar ales

package student_andredragu_se_labs_l2exemplul3;

class WhileDemo {
	
	// Functia main
    public static void main(String[] args) {
        int count = 1;										// Inceperea numaratorii de la 1
        while (count < 11) {								// Atat timp cat coount este mai mic decat 11...
            System.out.println("Count is: " + count);		// ... se va afisa count-ul...
            count++;										// ... si se va incrementa
        }
    }
}
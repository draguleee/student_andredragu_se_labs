// Exemplul 7, Laboratorul 4, sectiunea Reutilizarea Claselor
// Exemplu mostenire Persoana 2

package student_andredragu_se_labs_l4exemplul7;

public class Persoana {
    String nume;
    int varsta;  
 
    Persoana() {
        System.out.println("Constructor default persoana.");
    }
 
    Persoana(String nume, int varsta) {
        System.out.println("Constructor cu argumente persoana.");
        this.nume = nume;
        this.varsta = varsta;
    }
 
    void afiseazaDetalii() {
        System.out.println(nume+":"+varsta);
    }
}
 
class Student extends Persoana {
    String university;
 
    Student(String nume, int varsta, String uni) {
        super(nume,varsta);
        this.university = uni;
        System.out.println("Constructor student.");
    }
 
    void afiseazaDetalii() {
        super.afiseazaDetalii();
        System.out.println(university);
    }
}
 
class ErasmusStudent extends Persoana {
    String university;
    String remoteUniversity;
 
    ErasmusStudent(String nume, int varsta, String uni, String runi) {
        super(nume,varsta);
        this.university = uni;
        this.remoteUniversity = runi;
        System.out.println("Constructor student.");
    }
 
    void afiseazaDetalii() {
        super.afiseazaDetalii();
        System.out.println(university);
        System.out.println(remoteUniversity);      
    }
}
 

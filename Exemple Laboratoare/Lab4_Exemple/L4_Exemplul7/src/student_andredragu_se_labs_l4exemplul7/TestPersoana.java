package student_andredragu_se_labs_l4exemplul7;

public class TestPersoana {
	 
	   public static void main(String[] args) {
		   
	       Persoana p = new Persoana("A", 1);
	       Persoana s = new Student("B", 2, "UTCN");
	       Persoana e = new ErasmusStudent("C", 3, "UTCN", "UBB");
	       
	       p.afiseazaDetalii();
	       s.afiseazaDetalii();
	       e.afiseazaDetalii();
	 
	   } 
	}
// Exemplul 5, Laboratorul 4, sectiunea Reutilizarea Claselor
// Exemplu mostenire Vehicul

package student_andredragu_se_labs_l4exemplul5;

public class Vehicul {
    String nume;
 
    Vehicul(String nume) {
        this.nume = nume;
    }
 
    void afiseaza() {
        System.out.println("Vehicul nume: " + nume);
    }
 
    public static void main(String args[]) {
        Vehicul v1 = new Vehicul("A");
        Masina m1 = new Masina("B",1000);
        Vehicul m2 = new Masina("C",2000);
        m1.afiseaza();
        m2.afiseaza();
        v1.afiseaza();
    }
}
 
class Masina extends Vehicul{
    int cc;
 
    Masina(String nume, int cc){
        super(nume);
        this.cc = cc;
    }
 
    void afiseaza() {
        super.afiseaza();
        System.out.println("Vehicul cc: "+cc);
    }
}
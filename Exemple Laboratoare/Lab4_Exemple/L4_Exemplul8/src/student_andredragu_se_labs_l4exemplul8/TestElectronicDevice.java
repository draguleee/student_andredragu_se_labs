package student_andredragu_se_labs_l4exemplul8;

public class TestElectronicDevice {
	
    public static void main(String[] args) {
    	
        ElectronicDevice e1 = new ElectronicDevice();
        ElectronicDevice e2 = new TV();
 
        e1.turnOn();
        e1.turnOff();
 
        e2.turnOn();
        e2.turnOff();
 
        TV e3 = new TV();
        e3.turnOn();
        e3.channelUp();
 
        TV t1 = (TV)e2; //conversie de tip explicita
        t1.channelUp();
 
    }
}
// Exemplul 8, Laboratorul 4, sectiunea Reutilizarea Claselor
// Exemplu ElectronicDevice

package student_andredragu_se_labs_l4exemplul8;

public class ElectronicDevice {
    private boolean powered;
 
    public ElectronicDevice() {
        powered = false;
    }
 
 
    protected void turnOn() {
        System.out.println("Device turned ON");
        powered = true;
    }
 
    protected void turnOff() {
        System.out.println("Device turned OFF");
        powered = false;
    }
}
 
class TV extends ElectronicDevice {
     int channel;
 
    public TV() {}
 
    void channelUp() {
        if(channel<=10){
            channel++;
            System.out.println("Channel up "+channel);
        }
    }
 
    void channelDown() {
        if(channel>=2) {
            channel--;
            System.out.println("Channel down "+channel);
        }
    }
}
 
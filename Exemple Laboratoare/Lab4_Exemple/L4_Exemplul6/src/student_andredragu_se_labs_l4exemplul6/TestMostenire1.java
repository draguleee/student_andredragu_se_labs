// Exemplul 6, Laboratorul 4, sectiunea Reutilizarea Claselor
// Exemplu mostenire Persoana 1

package student_andredragu_se_labs_l4exemplul6;

public class TestMostenire1 {
	 
    public static void main(String[] args) {
        Persoana p1 = new Persoana();
        Persoana p2 = new Persoana("XYZ", 22);
        p1.afisare();
        p2.afisare();
 
        Student s1 = new Student("UTCN", "ABC", 23);
        s1.afisare();
 
        Persoana p3 = new Student("FGD", "CRL", 9);
        p3.afisare();
 
        Student s2 = new Student("UTCN", "ABC", 23);
        s1.afisare();
 
        Persoana x = s2;  //conversie de tip implicita
 
        Persoana p4 = new Student("FGD", "CRL", 9);
        p3.afisare();
 
        Student y = (Student) p4; //conversie de tip expilicita
 
        y.afiseazaSituatie();
 
        Persoana p5 = new Persoana();
 
        if (p5 instanceof Student) {
            Student z = (Student) p5;
            z.afiseazaSituatie();
        }
 
    }
}
 
class Persoana {
	String nume;
    int varsta;
 
    Persoana() {
        this.nume = "Fara Nume";
        this.varsta = -1;
    }
 
    Persoana(String nume, int varsta) {
        this.nume = nume;
        this.varsta = varsta;
    }
 
    void afisare() {
        System.out.println("Persoana " + nume + ":" + varsta);
    }
 
}
 
class Student extends Persoana {
	String uni;
 
    public Student(String uni, String nume, int varsta) {
        super(nume, varsta);
        this.uni = uni;
    }
 
    void afisare() {
        System.out.println("Student " + nume + ":" + varsta);
        System.out.println("Universitatea " + uni);
    }
 
    void afiseazaSituatie() {
        System.out.println("Afiseaza situatie student....");
    }
}
// Exemplul 4, Laboratorul 4, sectiunea Reutilizarea Claselor
// Exemplu Upcasting

package student_andredragu_se_labs_l4exemplul4;

public class EmployeeUpcasting {
	
	void doWork() {
		System.out.println("Employee do his job.");
	}
}
 
class ManagerUpcasting extends EmployeeUpcasting{
 
	void report(){
		System.out.println("Manager is reporting project status.");
	}
	
	void doWork() {
		System.out.println("Manager coordinate job.");
	}
 
}
 
class TestUpcasting {
	
	public static void main(String[] args) {		
		EmployeeUpcasting e = new ManagerUpcasting();
		e.doWork();
	}
}
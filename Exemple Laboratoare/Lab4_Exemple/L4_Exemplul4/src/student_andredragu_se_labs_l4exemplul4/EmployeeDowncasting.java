// Exemplul 4, Laboratorul 4, sectiunea Reutilizarea Claselor
// Exemplu Downcasting

package student_andredragu_se_labs_l4exemplul4;

public class EmployeeDowncasting {
	
	void doWork() {
		System.out.println("Employee do his job.");
	}
}
 
class ManagerDowncasting extends EmployeeDowncasting {
 
	void report() {
		System.out.println("Manager is reporting project status.");
	}
	
	void doWork() {
		System.out.println("Manager coordinate job.");
	}
 
}
 
 
class TestDowncasting {
	
	public static void main(String[] args) {		
		EmployeeDowncasting alin = new ManagerDowncasting();
		alin.doWork(); 
		((ManagerDowncasting)alin).report(); //1.
 
		EmployeeDowncasting dan = new EmployeeDowncasting(); //2.
		((ManagerDowncasting)dan).report(); //3.
 
	}

}
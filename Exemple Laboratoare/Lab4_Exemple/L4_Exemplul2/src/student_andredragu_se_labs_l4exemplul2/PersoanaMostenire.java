// Exemplul 2, Laboratorul 4, sectiunea Reutilizarea Claselor
// Clasa PersoanaMostenire
// implementarea unei clase ce mosteneste o alta clasa

package student_andredragu_se_labs_l4exemplul2;

public class PersoanaMostenire {	// Clasa PersoanaMostenire
    String nume;					// Numele persoanei, de tip String
    Adresa adr;						// Adresa persoanei, de tip Adresa
    
    void setAdresa(Adresa a) {		// Setter pentru adresa
    	adr = a;			
    }
    
    void afiseaza() {							// Metoda afiseaza()
    	System.out.println("Nume = " + nume);	// Afisarea numelui persoanei
    }          
}

class Angajat extends PersoanaMostenire {		// Clasa Angajat, care extinde clasa PersoanaMostenire
    int venit;			// Venitul angajatului
    
    void afiseaza() {								// Metoda afiseaza(), mostenita de la clasa PersoanaMostenire
    	super.afiseaza();	
    	System.out.println("Venit = " + venit);		// Afisarea venitului persoanei	
    }
    
    int calculeazaImpozit() {		// Metoda calculeazaImpozit()
    	return venit/2;
    }
}

class Adresa {			// Clasa Adresa, care este in relatie de compozitie cu clasa PersoanaMostenire
    String strada;		// Strada pe care locuieste persoana, de tip String	
    int nr;				// Numarul casei la care locuieste persoana, de tip int
}
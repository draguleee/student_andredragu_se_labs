// Exemplul 1, Laboratorul 4, sectiunea Specificatori de Acces
// Clasa RobotSpecAccess

package robot.machine;

public class RobotSpecAccess {			// Clasa RobotSpecAccess
    Engine robotEngine;					// Variabile robotEngine de tip Engine
    
    public RobotSpecAccess() {			// Constructor 
    	robotEngine = new Engine();		// Crearea unui nou motor
    }

    public void moveRobot(int x) {		// Metoda moveRobot(x)
    	robotEngine.step(x);			
    }
}

class Engine {			// Clasa Engine

    private void start() {						// Metoda start()
    	System.out.println("Start engine.");	// Afisare "Start engine"
    }

    private void checkDirection(int x) {					// Metoda checkDirection(x)
    	if(x < 0)											// Daca x este mai mic decat 0
    		System.out.println("Moving to the left.");		// Robotul se va deplasa la stanga
    	else if(x > 0)										// Daca x este mai mare decat 0
    		System.out.println("Moving to the right.");		// Robotul se va deplasa la dreapta
    }

    private void execute(int s) {							 		// Metoda execute(s)
    	System.out.println("Moving " + Math.abs(s) + " steps.");	// Afisarea deplasarii robotului 
    }
    
    private void stop() {						// Metoda stop()
    	System.out.println("Stop engine.");		// Oprirea robotului
    }
    
    void step(int x) {			// Metoda step()
    	start();				// Pornirea robotului
        checkDirection(x);		// Verificarea directiei, deplasare in functie de x
        execute(x);				// Numararea pasilor pe care ii face robotul
        stop();					// Oprirea robotului
    }

}
package robot.play;
import robot.machine.RobotSpecAccess;

public class BattleField {	// Clasa BattleField
	RobotSpecAccess r1;			// Variabila r1 de tip RobotSpecAccess
	RobotSpecAccess r2;			// Variabila r2 de tip RobotSpecAccess
	
	BattleField() {						// Constructor
		r1 = new RobotSpecAccess();		// Creare robot 1
		r2 = new RobotSpecAccess();		// Creare robot 2
	}
	
	public void play() {		// Metoda play()
		r1.moveRobot(15);		// Misca robotul in dreapta
		r2.moveRobot(-10);		// Misca robotul in stanga
	}
            
	public static void main(String[] args) {		// Metoda main()
		BattleField game = new BattleField();		// Crearea unui joc BattleField
		game.play();								// Pornire joc
	}
}
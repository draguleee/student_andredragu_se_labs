// Clasa Dictionary

package student_andredragu_se_labs_colocviu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.util.*;
 

public class Dictionary extends JFrame {		// Clasa Dictionary, care este o clasa de baza
	Word w;						// Variabila w de tip Word
    Definition d;				// Variabila d de tip Definition
    HashMap<Word,Definition> dictionary=new HashMap<Word,Definition>();		// Crearea unui HashMap care va avea ca si cheie Word, iar ca si valoare Definition
    
	JLabel cuvant,definitie;
	JTextField tCuvant,tDefinitie;
	JTextArea tArea;
	JButton Add;
	
	Dictionary(){
        setTitle("Dictionar Explicativ");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,300);
        setVisible(true);
	}
	
    public void init(){
    	 
        this.setLayout(null);
        int width=80;int height = 20;

        cuvant = new JLabel("Cuvantul: ");
        cuvant.setBounds(10, 50, width, height);

        definitie = new JLabel("Definitie ");
        definitie.setBounds(10, 100,width, height);

        tCuvant = new JTextField();
        tCuvant.setBounds(70,50,width, height);

        tDefinitie = new JTextField();
        tDefinitie.setBounds(70,100,width, height);

        Add = new JButton("Loghin");
        Add.setBounds(10,150,width, height);

        Add.addActionListener(new TratareAdd());

        tArea = new JTextArea();
        tArea.setBounds(10,180,150,80);

        add(cuvant);
        add(definitie);
        add(tCuvant);
        add(tDefinitie);
        add(Add);
        add(tArea);

    }
    
    class TratareAdd implements ActionListener {
    	 
        public void actionPerformed(ActionEvent e) {
        	
        }

    public void addWord(Word w, Definition d) {		// Metoda addWord(), care adauga un nou cuvant in dictionar
        dictionary.put(w,d);
    }

    public void getDefinition(Word w) {				// Metoda getDefinition(), care afiseaza definitia cuvantului
        for (Word word : dictionary.keySet()) {
            if (word.equals(w)) {
                System.out.println(dictionary.get(word).getDescription());
            }
        }
    }

    public void getAllWords() {						// Metoda getAllWords(), care afiseaza toate cuvintele din dictionar
        dictionary.forEach((w,d)-> System.out.println(w.getName()));
    }

    public void getAllDefinitions() {				// Metoda getAllDefinitions(), care afiseaza toate definitiile din dictionar
        dictionary.forEach((w,d)->System.out.println(d.getDescription()));
        }
    
    public void main(String[] args) {
    	Dictionary dictionar = new Dictionary();
    }
}

class Definition {	// Clasa Definition
    String description;		// Variabila description, de tip String
    
    Definition(){}			// Constructor implicit

    Definition(String d) {	// Constructor
        this.description = d;
    }

    public String getDescription() {		// Getter pentru descriere
        return description;
    }

    @Override 													
    public boolean equals(Object obj) {							// Metoda equals()
        Definition test = (Definition) obj;									
        return Objects.equals(test.description, description);
    }
}

class Word {			// Clasa Word
    String name;			// Cuvantul

    Word(){}	// Constructor implicit

    Word(String n) {		// Constructor
        this.name = n;
    }

    public String getName() {		// Getter pentru nume
        return name;
    }

    @Override
    public boolean equals(Object obj) {			// Metoda equals()
       Word w = (Word)obj;
       return Objects.equals(w.name,name);
    }
    
    @Override
    public int hashCode() {						// Metoda hashCode()
        return Objects.hash(name);
    }
}

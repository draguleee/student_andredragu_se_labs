// Clasa TestRobot
package student_andredragu_se_labs_l3ex1;

public class TestRobot {
	
    public static void main(String[] args) {
    	
    	Robot r1 = new Robot();		// Crearea unui nou robot
    	Robot r2 = new Robot();		// Crearea unui alt robot
    	
    	r1.change(10);			// Incrementare
    	r1.print();			// Afisarea pozitiei
    	System.out.println(" ");
    	r2.change(7);			// Incrementare
    	r2.print();			// Afisarea pozitiei
    	}
    
}
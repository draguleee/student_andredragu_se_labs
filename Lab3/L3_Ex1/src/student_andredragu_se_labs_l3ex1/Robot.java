// Clasa Robot

package student_andredragu_se_labs_l3ex1;

public class Robot {
	int x;			// Pozitia robotului

    Robot() {		// Constructorul default
    	x = 1;
    }
    
    void change(int k) {	// Metoda change(), care incrementeaza x
    	if(k>=1)			
    		x = k;			
    }  
    
    void print() {			// Metoda print()
    	System.out.println("Pozitia este: " + x);	
    }
}
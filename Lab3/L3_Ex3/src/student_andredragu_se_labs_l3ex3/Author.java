// Clasa Author

package student_andredragu_se_labs_l3ex3;

public class Author {
	
	private String name;		// Initializarea numelui autorului
	private String email;		// Initializare e-mailului autorului
	private char gender;		// Initializare sexului autorului
	
	public Author(String n, String e, char g) {		// Constructor cu this keyword
		this.name = n;			
		this.email = e;			
		this.gender = g;		
	}
	
	public void getName() {		// Getter pentru numele autorului
		System.out.println("Numele autorului este: " + this.name);
	}
	
	public void getEmail() {		// Getter pentru e-mailul autorului
		System.out.println("E-mailul autorului este: " + this.email);
	}
	
	public void setEmail(String email) {		// Setter pentru email
		this.email = email;
	}
	
	public void getGender() {		// Setter pentru sexul autorului
		System.out.println("Sexul autorului este: " + this.gender);
	}
	
	void print() {		// Metoda print
		System.out.println(this.name + " (" + this.gender + ") " + "at " + this.email);
	}
	
}

// Clasa TestAuthor

package student_andredragu_se_labs_l3ex3;

public class TestAuthor {
	
	public static void main(String[] args) {
		
	Author a1 = new Author("Andre", "aandreid14@gmail.com", 'f');		// Autor nou
	Author a2 = new Author("Malina", "mali_pretty27@gmail.com", 'f');	// Autor nou
	
	a1.getName();		// Returnare nume primul autor
	a1.getEmail();		// Returnare e-mail primul autor
	a1.setEmail("aandreid14@gmail.com");
	a1.getGender(); 	// Returnare sex primul autor
	a1.print();
	System.out.println(" ");
	a2.getName();		// Returnare nume al doilea autor
	a2.getEmail(); 		// Returnare e-mail al doilea autor
	a2.setEmail("mali_pretty27@gmail.com");
	a2.getGender(); 	// Returnare sex al doilea autor
	a2.print();
	
	}	
	
}

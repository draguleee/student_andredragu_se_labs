// Sa se modifice clasa Flower astfel incae sa mentina numarul de obiecte construite.
// Sa se adauge o metoda care returneaza numarul obiectelor construite.
// Se vor folosi variabile statice si metode statice.

package student_andredragu_se_labs_l3ex5;

import java.util.Scanner;

public class Flower {
	
    int petal;					// Initializare petal
    static int count = 0;		// Initializare count - variabila ce pastreaza numarul de obiecte create
    Scanner in = new Scanner(System.in);
    
    Flower() { 					// Constructorul pentru floare
    	System.out.println("Flower has been created!");            
    }
    
    static int numObjects() {	// Metoda pentru numararea obiectelor create
    	 count++;
    	 return count;
    }

    public static void main(String[] args) {	// Functia main
    	Flower[] garden = new Flower[5];
    	for(int i = 0; i < 5; i++) {
    		Flower f = new Flower();
    		garden[i] = f;
    		f.numObjects();
    	}
    	
		System.out.println("Numarul de obiecte create: " + count);
    	
    }
    
}
// Clasa MyPoint.
// Scrieti o clasa TestMyPoint care testeaza toate metodele din clasa MyPoint.

package student_andredragu_se_labs_l3ex4;
import java.lang.Math;
import java.util.Scanner;

public class MyPoint {
	
	int x;		// Initializare coordonata x
	int y;		// Initializare coordonata y
	
	public MyPoint() {					// Constructor fara argumente
		x = 0;
		y = 0;
	}
	
	public MyPoint(int x, int y) {		// Constructor cu argumente
		this.x = x;
		this.y = y;
	}
	
	public void setX(int a) {			// Setter pentru coordonata x
		x = a;
	}
	
	public int getX() {					// Getter pentru coordonata x
		return x;
	}
	
	public void setY(int b) {			// Setter pentru coordonata y
		y = b;
	}
	
	public int getY() {					// Getter pentru coordonata y
		return y;
	}
	
	public void setXY(int a, int b) {	// Setter pentru coordonatele x,y
		x = a;
		y = b;
	}
	
	public String toString() {			// Metoda ToString
		return "("+x+", "+y+")";
	}
	
	public double distance1(int a, int b) {			// Metoda distanta 1
		double rad = Math.sqrt((a-x)^2 + (b - y)^2);
		return rad;
	}
	
	public double distance2(MyPoint another) {		// Metoda distanta 2
		double rad = Math.sqrt((this.x - another.x)^2 + (this.y - another.y)^2);
		return rad;
	}
	
	public static void testMyPoint() {				// Metoda testMyPoint
		
		// Testare constructor fara argumente
		Scanner in = new Scanner(System.in);
		System.out.println();
		MyPoint A = new MyPoint();
		System.out.println("Punctul A: " + A);
		
		// Testare constructor cu argumente
		System.out.println();
		System.out.print("X = ");
		int x1 = in.nextInt();
		System.out.print("Y = ");
		int y1 = in.nextInt();
		MyPoint B = new MyPoint(x1,y1);
		System.out.println("Punctul B: " + B);
		
		// Testare setX, setY, getX, getY
		System.out.println();
		System.out.print("xA = ");
		int x2 = in.nextInt();
		A.setX(x2);
		System.out.print("yA = ");
		int y2 = in.nextInt();
		A.setY(y2);
		System.out.println("Punctul A modificat: (" + A.getX()+", "+A.getY()+")");
		
		// Testare getXY
		System.out.println();
		System.out.print("xB = ");
		int x3 = in.nextInt();
		System.out.print("yB = ");
		int y3 = in.nextInt();
		B.setXY(x3,y3);
		System.out.println("Punctul B modificat:" + B);
		
		// Testare distanta 1
		System.out.println();
		System.out.print("xC = ");
		int x4 = in.nextInt();
		System.out.print("yC = ");
		int y4 = in.nextInt();
		System.out.println("Punctul C: (" + x4+", "+y4+")");
		System.out.println("Distanta de la A la C = " + A.distance1(x4,y4));
		
		// Testare distanta 2
		System.out.println();
		System.out.print("xD = ");
		int x5 = in.nextInt();
		System.out.print("yD = ");
		int y5 = in.nextInt();
		MyPoint D = new MyPoint(x5,y5);
		System.out.println("Punctul D:" + D);
		System.out.println("Distanta de la A la D = " + A.distance2(D));
	}

	public static void main(String[] args) {		// Functia main
		testMyPoint();
	}

}

// Clasa TestCircle

package student_andredragu_se_labs_l3ex2;

public class TestCircle {
	
	public static void main (String[] args) {
		
		Circle c1 = new Circle();		// Crearea unui nou cerc cu primul constructor
		Circle c2 = new Circle(8);		// Crearea unui alt cerc cu al doilea constructor
		
		c1.getRadius();			// Returnare raza pentru primul cerc
		c1.getArea();			// Returnare arie pentru primul cerc
		c1.getColour();			// Returnare culoare pentru primul cerc
		System.out.println(" ");
		c2.getRadius();			// Returnare raza pentru al doilea cerc
		c2.getArea();			// Returnare arie pentru al doilea cerc
		c2.getColour();			// Returnare culoare pentru al doilea cerc
	}
	
}

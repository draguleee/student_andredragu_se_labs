// Clasa Circle

package student_andredragu_se_labs_l3ex2;

public class Circle {
	
	private double radius = 1.0;		// Raza cercului
	private String color = "red";		// Culoarea cercului
	
	Circle() {					// Constructor suprainscris
		this.radius = 2.5;		// Raza cercului definitia in cadrul constructorului suprainscris
		this.color = "brown";	// Culoarea cercului definita in cadrul constructorului suprainscris
	}
	
	Circle(double x) {			// Constructor suprainscris
		this.radius = x;		// Atribuirea unei alte valori pentru raza cercului in cadrul acestui constructor
	}
	
	void getRadius() {		// Getter pentru raza
		System.out.println("Raza cercului este: " + this.radius);
	}
	
	void getArea() {		// Getter pentru arie
		System.out.println("Aria cercului este: " + Math.PI * this.radius * this.radius);		// Calculul si afisarea ariei cercului
	}
	
	void getColour() {		// Getter pentru culoare
		System.out.println("Culoarea este: " + this.color);
	}
	
}

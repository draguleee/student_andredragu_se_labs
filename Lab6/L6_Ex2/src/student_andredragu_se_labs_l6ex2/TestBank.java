// Clasa TestBank

package student_andredragu_se_labs_l6ex2;

import java.util.ArrayList;
import java.awt.List;
import java.util.*;

public class TestBank {				// Clasa TestBank
	
    public static void sorting(ArrayList <BankAccount> accounts) {				// Metoda pentru sortare alfabetica dupa detinator
        for(int i = 0; i < accounts.size() - 1; i++) {				
            for (int j = i + 1; j < accounts.size(); j++) {
                if ((accounts.get(i).getOwner().compareTo(accounts.get(j).getOwner())) > 0) {
                    String o1 = (String) accounts.get(i).getOwner();
                    double b1 = (double) accounts.get(i).getBalance();

                    accounts.get(i).setOwner(accounts.get(j).getOwner());
                    accounts.get(i).setBalance(accounts.get(j).getBalance());

                    accounts.get(j).setOwner(o1);
                    accounts.get(j).setBalance(b1);
                }
            }
        }
        for(int k = 0; k < accounts.size(); k++) {
        	System.out.println(accounts.get(k).getOwner());
        }
        
    }
    
    public static void main(String[] args) {
        Bank bank = new Bank();							// Crearea unei banci
        
        bank.addAccount("Alexandra", 10000);			// Creare cont nou pe numele Alexandra
        bank.addAccount("Rares", 12500);				// Creare cont nou pe numele Rares
        bank.addAccount("Anastasia", 75000);			// Creare cont nou pe numele Anastasia
        bank.addAccount("Oana", 30000);					// Creare cont nou pe numele Oana
        bank.addAccount("Amalia", 50000);				// Creare cont nou pe numele Amalia
        bank.printAccounts();							// Afisarea tuturor conturilor create
        
        System.out.println(" ");
        
        System.out.println("Detinatorii de cont bancar cu suma intre 10000 si 35000 sunt: ");	
        bank.printAccounts(10000, 35000);				// Afisarea tuturor conturilor bancare cuprinse intre 10000 si 35000
        
        System.out.println(" ");
        
        System.out.println("Conturile bancare sortate in ordine alfabetica dupa detinator sunt: ");
        sorting(bank.getAllAccounts());					// Sortarea si returnarea conturilor in ordine alfabetica

    }

}
// Clasa Bank

package student_andredragu_se_labs_l6ex2;

import java.util.ArrayList;
import java.awt.List;
import java.util.*;

public class Bank {							// Clasa Bank
    ArrayList <BankAccount> accounts = new ArrayList();			// ArrayList pentru stocarea obiectelor BankAccount
    int x;

    public void addAccount(String o, double b) {	// Metoda addAccount pentru adaugarea conturilor
        BankAccount ba = new BankAccount(o,b);		// Creare cont nou
        accounts.add(ba);							// Adaugare cont
    }

    public void printAccounts() {	// Afisare conturi sortate dupa nume
    	do {
    		x = 1;
    		for (int i = 0; i < accounts.size() - 1; i++) {				// Parcurgerea listei de conturi bancare
    			BankAccount ba1 = (BankAccount) accounts.get(i);		// Creare cont bancar 1
                BankAccount ba2 = (BankAccount) accounts.get(i + 1);	// Creare cont bancar 2
                
                if (ba1.getBalance() > ba2.getBalance()) {				
                	
                    double alfa = ba1.getBalance();
                    ba1.setBalance(ba2.getBalance());
                    ba2.setBalance(alfa);

                    String name = ba1.getOwner();
                    ba1.setOwner(ba2.getOwner());
                    ba2.setOwner(name);

                    x = 0;
                }
            }
        }
    	while(x == 0);

        for(int j = 0; j < accounts.size(); j++)
        	System.out.println(accounts.get(j).toString());
    }

    public void printAccounts(double minBalance, double maxBalance) {	// Afisare conturi intre minBalance si maxBalance
    	for(int k = 0; k < accounts.size(); k++) {						// Parcurgerea listei de conturi bancare 
                BankAccount x = (BankAccount) accounts.get(k);
                if (x.getBalance() > minBalance && x.getBalance() < maxBalance)
                	System.out.println(x.toString());
        }

    }
    
    public ArrayList getAllAccounts() {									// Getter pentru conturile bancare
            return accounts;
    }

}
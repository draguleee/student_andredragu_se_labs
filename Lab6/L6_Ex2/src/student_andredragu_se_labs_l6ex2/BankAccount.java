// Clasa BankAccount

package student_andredragu_se_labs_l6ex2;

public class BankAccount {
	private String owner;
	private double balance;
	
	public BankAccount() {						// Constructor
		this.owner = "Andreea Ioana Dragu";
		this.balance = 20000.0;
	}
	
	public BankAccount(double b) {
		this.balance = b;
	}
	
	public BankAccount(String o, double b) {	// Constructor
		this.owner = o;
		this.balance = b;
	}
	
	public String getOwner() {					// Getter pentru owner
		return this.owner;
	}
	
	public void setOwner(String o) {			// Setter pentru owner
		this.owner = o;
	}
	
	public double getBalance() {				// Getter pentru balance
		return this.balance;
	}
	
	public void setBalance(double b) {			// Setter pentru balance
		this.balance = b;
	}
	
	public void withdraw(double a) {			// Calcul withdraw
		this.balance = this.balance - a;
	}
	
	public void deposit(double a) {				// Calcul deposit
		this.balance = this.balance + a;
	}
	
	@Override
	public boolean equals(Object obj) {			// Metoda equals (override)
		if(obj==null || !(obj instanceof BankAccount))
			return false;
		BankAccount ba = (BankAccount)obj;
		return balance == ba.balance && ba.owner.equals(owner);
	}
	
	@Override
	public int hashCode() {						// Metoda hashCode (override)
		return (int)balance + owner.hashCode();
	}
	
	@Override
	public String toString() {					// Metoda toString (override)
		return "BankAccount: [owner = " + this.owner + "," + " balance = " + this.balance + "]";
	}
	
	public int compareTo(Object o) {			// Metoda compareTo pentru Object
		BankAccount test = (BankAccount)o;
		if(test.balance > balance)
			return 1;
		if(test.balance == balance)
			return 0;
		return -1;
	}
	
	public int compareTo(String string) {		// Metoda compareTo pentru String
		return this.owner.compareTo(string);
	}
	
}

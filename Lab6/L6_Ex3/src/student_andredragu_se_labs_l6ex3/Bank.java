// Clasa Bank cu TreeSet

package student_andredragu_se_labs_l6ex3;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;
import java.util.*;

public class Bank {

    TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();
    TreeSet<BankAccount> sorted1 = new TreeSet<BankAccount>(new BalanceComparator());
    TreeSet<BankAccount> sorted2 = new TreeSet<BankAccount>(new OwnerComparator());

    public void addAccount(String owner, double balance) {
        BankAccount A = new BankAccount(owner, balance);
        accounts.add(A);
    }

    public void printAccounts1() {
        sorted1.addAll(accounts);
        System.out.println(sorted1);
        }

    public void printAccounts2() {
    	sorted2.addAll(accounts);
    	System.out.println(sorted2);
    }

    public void printAccounts(double minBalance, double maxBalance){
        BankAccount a = new BankAccount(minBalance);
        BankAccount b = new BankAccount(maxBalance);
        for(BankAccount test:accounts){
            BankAccount please=(BankAccount)test;
            if(please.getBalance()>minBalance && please.getBalance()<maxBalance)
                System.out.println(please);
        }
    }

    public Set getAllAccounts(){
            return accounts;
    }
}




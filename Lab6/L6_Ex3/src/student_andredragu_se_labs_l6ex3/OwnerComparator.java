package student_andredragu_se_labs_l6ex3;

import java.util.Comparator;

public class OwnerComparator implements Comparator<BankAccount> {
    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        return o1.getOwner().compareTo(o2.getOwner());
    }
}

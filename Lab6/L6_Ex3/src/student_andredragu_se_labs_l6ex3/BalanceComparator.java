package student_andredragu_se_labs_l6ex3;

import java.util.Comparator;

public class BalanceComparator implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount o1, BankAccount o2) {
        if(o1.getBalance()>o2.getBalance())
            return 1;
        else return -1;
    }
}

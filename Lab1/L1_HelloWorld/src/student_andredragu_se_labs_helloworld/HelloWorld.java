// Program Hello World

package student_andredragu_se_labs_helloworld;

public class HelloWorld {
    public static void main(String[] args) {
        // Prints "Hello, World" to the terminal window.
        System.out.println("Hello, World");
    }
    
}
